# Product Backlog

## Team Members
Bryan Duffy, Mike Reed, Ana Hill, Roy Fu, Jacob Stuart, Karan Aggarwal

## Problem Statement
The sharing economy is booming, but there are still many industries that can benefit from this new approach to business. Storage is one of those areas, as there are many consumers who have long-term storage demands that struggle to find a suitable storage facility or wish for more flexibility in how much they pay for storage. By taking an AirBnB approach to long-term storage, we can provide this flexibility and convenience to consumers with long-term storage needs. Home-owners can also utilize unused space in their house to generate passive income as well. We aim to be different from similar services by focusing on seasonal,  low-cost rentals aimed at college students - similar services such as Roost focus on large cities and even then do not have large market ownership.

## Requirements

### Functional:
1. As a user, I would like to create an account with personal information
2. As a user, I would like to view all available options for storage in my area
3. As a user, I would like to be able to post my own storage space
4. As a user, I would like to filter my storage options to various criteria
5. As a user, I would like to be able to be reimbursed per month for storage
6. As a user, I would like to see a description, rating, and pictures of a storage area
7. As a user, I would like to add pictures for a storage locker
8. As a user, I would like to be able to post ratings and reviews on storage options
9. As a user, I would like to log in to my account
10. As a user, I would like to receive important notifications by email
11. As a user, I would like to upload an avatar
12. As a user, I want to be able to report unsafe/unjust storage conditions
13. As a user, I would like to securely associate payment information with my account to facilitate easy purchases
14. As a user, I would like to be able to send and receive private messages with sellers or buyers
15. As a user, I would like to look at past rentals made
16. As a user, I would like to create custom rental agreements for my storage space
17. As a user, I would like to be able to create an automated monthly payment plan
18. As a user, I would like to be able to request pictures of my stored items to ensure their safety
19. As a user, I would like to view other user’s profiles
20. As a user, I would like to be able to report a bug
21. As a user, I would like to have options in type of storage I would like to rent
22. As a user, I would like to have the capability of paying with credit card
23. As a user, I would like to be able to opt for SMS text notifications
24. As a user, I would like to schedule my rental period
25. As a user, I would like to view a calendar of my rentals
26. As a user, I would like to share my phone number with other users
27. As a user, I would like to create a description for my listing
28. As a user, I would like to be able to set the price and time availability on my storage space
29. As an administrator, I would like to be able to ban users for improper use
30. As a user, I would like to be able to block other users

### Non-Functional:
* Must integrate with Google Maps
* Easy to navigate UI
* Simple signup process
* Background checks done after creating account
* Aesthetically pleasing and simple to use interface for viewing listings
* Run payments through Stripe
