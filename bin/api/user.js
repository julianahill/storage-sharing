import express from 'express';
import randomstring from 'randomstring';
import { firebase, admin } from '../modules/firebase';
import mailgunClient from '../modules/mailgun';
import encrypter from '../modules/encrypter';

const router = express.Router();

router.get('/', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      const uid = decodedToken.uid;
      return admin.database().ref(`/users/${uid}`).once('value');
    }).then((snapshot) => {
      res.json({
        result: true,
        user: Object.assign({}, snapshot.val(), { id: snapshot.key }),
      });
    })
    .catch((error) => {
      res.json({ result: false, message: error.message });
    });
});

router.get('/:uid', (req, res) => {
  const token = req.header('Authorization');
  let myUid = '';
  let uid = '';
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      uid = req.params.uid;
      myUid = decodedToken.uid;
      return admin.database().ref(`/users/${uid}`).once('value');
    }).then((snapshot) => {
      const info = snapshot.val();
      let phonePermission = myUid === uid;
      console.log(`default phonePermission ${phonePermission}`);
      admin.database().ref(`/users/${uid}/phones`).on('value', (snap) => {
        snap.forEach((phone) => {
          phonePermission = phone.val() === myUid || phonePermission;
        });
      });
      info.phone = phonePermission ? info.phone : null;
      res.json({
        result: true,
        user: Object.assign({}, info, { id: snapshot.key }),
      });
    })
    .catch((error) => {
      res.json({ result: false, message: error.message });
      console.log(error.message);
    });
});

router.put('/', (req, res) => {
  const token = req.header('Authorization');
  let uid;
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      uid = decodedToken.uid;
      return admin.database().ref(`/users/${uid}`).update(req.body);
    })
    .then(() => (admin.database().ref(`/users/${uid}`).once('value')))
    .then((snapshot) => {
      res.json({
        result: true,
        user: Object.assign({}, snapshot.val(), { id: snapshot.key }),
      });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false });
    });
});

router.put('/', (req, res) => {
  const token = req.header('Authorization');
  let uid;
  let email;
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      uid = decodedToken.uid;
      email = decodedToken.email;
      return admin.database().ref(`/users/${uid}`).update(req.body);
    })
    .then(() => {
      return admin.database().ref(`/users/${uid}`).once('value');
    })
    .then((snapshot) => {
      res.json({
        result: true,
        user: Object.assign({}, snapshot.val(), { id: snapshot.key }),
      });
      mailgunClient.messages().send({
        from: 'StoreStuff <mailgun@sandboxde821aab0c504b59be6fe27cbaf151d6.mailgun.org>',
        to: email,
        subject: 'A Message from StoreStuff',
        text: 'We just wanted to let you know that your information has been updated!',
      }, (error) => {
        if (error) {
          console.log(error);
        }
      });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false });
    });
});

router.put('/update', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const email = req.body.email === '' ? null : req.body.email;
    const photoURL = req.body.photoURL === '' ? null : req.body.photoURL;
    const displayName = req.body.user === '' ? null : req.body.user;
    const password = req.body.password === '' ? null : req.body.password;
    const address = req.body.address === '' ? null : req.body.address;
    const creditcard = req.body.creditcard === '' ? null : req.body.creditcard;

    admin.database().ref(`/users/${uid}`).update({
      address,
      creditcard,
    }).catch((error) => {
      res.json({ result: false, message: error.message });
    });
    admin.auth().updateUser(uid, {
      email,
      password,
      displayName,
      photoURL,
    }).then((user) => {
      res.json({ result: true, message: user.toJSON() });
    })
    .catch((error) => {
      res.json({ result: false, message: error.message });
    });
  });
});

router.post('/create', (req, res) => {
  firebase.auth().createUserWithEmailAndPassword(req.body.username, req.body.password).then(() => {
    const currentUser = firebase.auth().currentUser;
    const userRef = firebase.database().ref(`/users/${currentUser.uid}`);
    userRef.set({
      firstName: req.body.firstName,
      lastName: req.body.lastName,
      username: currentUser.email,
      hasTempPassword: false,
    }).then(() => {
      mailgunClient.messages().send({
        from: 'StoreStuff <mailgun@sandboxde821aab0c504b59be6fe27cbaf151d6.mailgun.org>',
        to: currentUser.email,
        subject: 'Welcome to StoreStuff!',
        text: 'Thanks for joining StoreStuff! We hope to help you find a place to store your stuff!.',
      }, (error) => {
        if (error) {
          console.log(error);
        }
      });
      userRef.once('value', (snapshot) => {
        currentUser.getToken().then((token) => {
          res.json({
            result: true,
            user: Object.assign({}, snapshot.val(), { id: snapshot.key }),
            token,
          });
        });
      });
    });
  }).catch((error) => {
    res.json({ result: false, message: error.message });
  });
});

router.delete('/delete', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    admin.database().ref(`/users/${uid}`).remove()
    .catch((error) => {
      res.json({ result: false, message: error.message });
    });
    admin.auth().deleteUser(uid).then(() => {
      res.json({ result: true, message: 'User deleted.' });
    })
    .catch((error) => {
      console.log('failed');
      res.json({ result: false, message: error.message });
    });
  });
});

router.post('/resetpassword', (req, res) => {
  const tempPassword = randomstring.generate(8);
  admin.auth().getUserByEmail(req.body.email)
    .then(userRecord => (
      admin.auth().updateUser(userRecord.uid, {
        password: tempPassword,
      })
      .then((user) => {
        firebase.database().ref(`/users/${user.uid}`).set({
          username: user.email,
          hasTempPassword: true,
        });
        return user;
      })
      .then((user) => {
        mailgunClient.messages().send({
          from: 'StoreStuff <mailgun@sandboxde821aab0c504b59be6fe27cbaf151d6.mailgun.org>',
          to: user.email,
          subject: 'A Message from StoreStuff!',
          text: `Here's your temporary password: ${tempPassword}`,
        }, (error) => {
          if (error) {
            console.log(error); // TODO: Send error to client
          }
        });
      })
      .then(() => {
        res.json({ result: true });
      })
    ))
    .catch((error) => {
      res.json({ result: false, message: error.message });
    });
});

router.get('/:uid/phone', (req, res) => {
  const uid = req.params.uid;
  const token = req.header('Authorization');
  let msg = '';
  let myUid = '';
  let email = '';
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      myUid = decodedToken.uid;
      return admin.database().ref(`/users/${decodedToken.uid}`).once('value');
    })
    .then((snap) => {
      const me = snap.val();
      const reqRef = admin.database().ref('/phoneRequests');
      email = me.username;
      const r = reqRef.push({
        src: myUid,
        dest: uid,
      });
      msg = `
        Hi there!
        ${email} would like to request your phone number.
        Go to http://localhost:3001/api/req/${r.key}/accept to share it.
      `;
      return admin.database().ref(`/users/${uid}`).once('value');
    })
    .then((snap) => {
      const msgRef = admin.database().ref('/messages');
      msgRef.push({
        sender: email,
        recipient: snap.val().username,
        title: 'What\'s your number?',
        body: encrypter.encrypt(msg),
        timestamp: Date.now(),
      });
      mailgunClient.messages().send({
        from: 'StoreStuff <mailgun@sandboxde821aab0c504b59be6fe27cbaf151d6.mailgun.org>',
        to: snap.val().username,
        subject: 'You have a new message on StoreStuff!',
        text: 'Please go to StoreStuff to view your new message.!.',
      });
      res.json({ result: true });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
});


router.get('/:uid/block', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const srcEmail = decodedToken.username || decodedToken.email;
    const src = decodedToken.uid;
    const tar = req.params.uid;
    admin.database().ref(`/users/${tar}/username`).once('value').then((emailSnap) => {
      const tarEmail = emailSnap.val();
      let r = admin.database().ref(`/users/${src}/blocked`);
      r.push(tar);
      r.push(tarEmail);
      r = admin.database().ref(`/users/${tar}/blockedBy`);
      r.push(src);
      r.push(srcEmail);
      res.json({ res: true });
    });
  }).catch(() => res.json({ res: false }));
});

router.get('/:uid/ban', (req, res) => {
  const uid = req.params.uid;
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      return admin.database().ref(`/users/${decodedToken.uid}`).once('value');
    })
    .then((snapshot) => {
      if (snapshot.val().admin) {
        return admin.database().ref(`/users/${uid}`).once('value');
      }
      return Promise.reject(new Error('not admin'));
    })
    .then((snapshot) => {
      const up = snapshot.val();
      up.banned = true;
      return admin.database().ref(`/users/${uid}`).update(up);
    })
    .then(() => res.json({ result: true }))
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
});

module.exports = router;
