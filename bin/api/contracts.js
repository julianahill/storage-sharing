/* eslint no-console: 0 */
import express from 'express';
import { admin } from '../modules/firebase';
import stripe from '../modules/stripe';

const router = express.Router();

router.get('/', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const email = decodedToken.email;
    const ownerContracts = [];
    const renterContracts = [];
    admin.database().ref('/contracts').once('value').then((snapshot) => {
      const promises = [];
      snapshot.forEach((childSnapshot) => {
        if (childSnapshot.val().ownerId === uid) {
          promises.push(admin.auth().getUser(childSnapshot.val().renterId)
            .then(renterRecord => (
              admin.database().ref(`/postings/${childSnapshot.val().postingId}`).once('value')
              .then((postingSnap) => {
                ownerContracts.push(Object.assign(
                  {},
                  childSnapshot.val(),
                  { id: childSnapshot.key },
                  { ownerName: email },
                  { renterName: renterRecord.toJSON().email },
                  { postingTitle: postingSnap.val().title },
                ));
              })
              .catch((error) => {
                console.log(`Find Posting Error -> ${error}`);
                res.json({ result: false, message: error.message });
              })
            )));
        } else if (childSnapshot.val().renterId === uid) {
          promises.push(admin.auth().getUser(childSnapshot.val().ownerId)
            .then(ownerRecord => (
            admin.database().ref(`/postings/${childSnapshot.val().postingId}`)
            .once('value')
            .then((postingSnap) => {
              renterContracts.push(Object.assign(
                {},
                childSnapshot.val(),
                { id: childSnapshot.key },
                { ownerName: ownerRecord.toJSON().email },
                { renterName: email },
                { postingTitle: postingSnap.val().title },
              ));
            })
            .catch((error) => {
              console.log(`Find Posting error here -> ${error}`);
              res.json({ result: false, message: error.message });
            })
          )));
        }
      });
      return Promise.all(promises);
    })
    .then(() => {
      res.json({
        result: true,
        ownerContracts,
        renterContracts,
      });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
  })
  .catch((error) => {
    console.log(error);
    res.json({ result: false, messge: error.message });
  });
});

router.get('/:id', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const email = decodedToken.email;
    admin.database().ref(`/contracts/${req.params.id}`).once('value').then((snapshot) => {
      if (snapshot.val().ownerId === uid) {
        admin.auth().getUser(snapshot.val().renterId)
          .then(renterRecord => (
            admin.database().ref(`/postings/${snapshot.val().postingId}`).once('value')
            .then((postingSnap) => {
              res.json({
                result: true,
                contract: Object.assign(
                  snapshot.val(),
                  { id: snapshot.key },
                  { ownerName: email },
                  { renterName: renterRecord.toJSON().email },
                  { postingTitle: postingSnap.val().title },
                ),
              });
            })
            .catch((error) => {
              console.log(`Find Posting Error -> ${error}`);
              res.json({ result: false, message: error.message });
            })
          ));
      } else if (snapshot.val().renterId === uid) {
        admin.auth().getUser(snapshot.val().ownerId)
          .then(ownerRecord => (
          admin.database().ref(`/postings/${snapshot.val().postingId}`)
          .once('value')
          .then((postingSnap) => {
            res.json({
              result: true,
              contract: Object.assign(
                {},
                snapshot.val(),
                { id: snapshot.key },
                { ownerName: ownerRecord.toJSON().email },
                { renterName: email },
                { postingTitle: postingSnap.val().title },
              ),
            });
          })
          .catch((error) => {
            console.log(`Find Posting error -> ${error}`);
            res.json({ result: false, message: error.message });
          })
        ));
      }
    });
  });
});

router.post('/create', (req, res) => {
  const db = admin.database();
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const userRef = db.ref(`/users/${uid}`);
    return userRef.once('value');
  })
  .then((userSnap) => {
    const user = userSnap.val();
    if (!user.stripeCustId) {
      throw Error('No card saved for user');
    }
    const contractsRef = db.ref('/contracts');
    const payload = {
      startDate: req.body.startDate,
      endDate: req.body.endDate,
      postingId: req.body.postingId,
      rate: req.body.rate,
      frequency: req.body.frequency,
      ownerId: req.body.ownerId,
      renterId: userSnap.key,
      accepted: false,
    };
    const newContractRef = contractsRef.push();
    newContractRef.set(payload)
    .catch((error) => {
      console.log(`Error Pushing Contract -> ${error}`);
      res.json({ result: false, message: error.message });
    });
    newContractRef.once('value').then((snapshot) => {
      const contract = (Object.assign({}, snapshot.val(), { contractId: snapshot.key }));
      res.json({ result: true, contract });
    })
    .catch((error) => {
      res.json({ result: false, message: error.message });
    });
  })
  .catch((error) => {
    console.log(`VerifyIdToken Error -> ${error}`);
    res.json({ result: false, message: error.message });
  });
});

router.get('/:contract/accept', (req, res) => {
  const token = req.header('Authorization');
  const contractId = req.params.contract;
  const db = admin.database();
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    return uid;
  })
  .then((uid) => {
    const contractRef = db.ref(`/contracts/${contractId}`);
    return Promise.all([uid, contractRef.once('value'), contractRef]);
  })
  .then((result) => {
    const uid = result[0];
    const snap = result[1];
    const contractRef = result[2];
    const contract = snap.val();
    if (!contract) {
      throw Error('nonexistent agreement');
    } else if (!(uid === contract.ownerId)) {
      console.log(uid);
      console.log(contract.ownerId);
      throw Error('contract auth failure');
    }
    contract.accepted = true;
    contractRef.set(contract);
    const ownerRef = db.ref(`/users/${contract.ownerId}`);
    const renterRef = db.ref(`/users/${contract.renterId}`);
    return Promise.all([
      snap,
      ownerRef.once('value'),
      renterRef.once('value'),
    ]);
  })
  .then((results) => {
    const contract = results[0].val();
    const owner = results[1].val();
    const renter = results[2].val();
    const stripeConnectConfig = {
      stripe_account: owner.stripeId,
    };
    return Promise.all([
      stripe.plans.create({
        amount: contract.rate * 100,
        interval: 'month',
        name: `${renter.firstName} ${renter.lastName} ${results[0].key}`,
        currency: 'USD',
        id: `${renter.firstName}-${renter.lastName}${results[0].key}`,
      },
        stripeConnectConfig,
      ),
      stripe.tokens.create({
        customer: renter.stripeCustId,
      },
        stripeConnectConfig,
      ),
      stripeConnectConfig,
    ]);
  })
  .then((results) => {
    const plan = results[0];
    const stripeToken = results[1];
    const stripeConnectConfig = results[2];
    return Promise.all([
      plan,
      stripe.customers.create({
        description: 'Customer for storestuff.com',
        source: stripeToken.id,
      },
        stripeConnectConfig,
      ),
      stripeConnectConfig,
    ]);
  })
  .then(results => (
    stripe.subscriptions.create({
      customer: results[1].id,
      plan: results[0].id,
    }, results[2])
  ))
  .then(() => res.json({ result: true }))
  .catch((error) => {
    console.log(error);
    res.json({ result: false });
  });
});

module.exports = router;
