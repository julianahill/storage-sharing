import express from 'express';
import { firebase, admin } from '../modules/firebase';
import mailgunClient from '../modules/mailgun';
import encrypter from '../modules/encrypter';

const router = express.Router();

router.get('/', (req, res) => {
  const token = req.header('Authorization');

  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const ignore = [];
    admin.database().ref(`/users/${uid}/blocked`).once('value').then((blocks) => {
      blocks.forEach((block) => {
        ignore.push(block.val());
      });
    })
    .then(() => {
      const sentMessages = [];
      const receivedMessages = [];
      admin.auth().getUser(uid).then((userRecord) => {
        const record = userRecord.toJSON();
        const messageRef = firebase.database().ref('/messages/');
        messageRef.once('value', (snapshot) => {
          snapshot.forEach((childSnapshot) => {
            if (childSnapshot.val().sender === record.email) {
              sentMessages.push(Object.assign(
                {},
                childSnapshot.val(),
                { body: encrypter.decrypt(childSnapshot.val().body) },
              ));
            }
            if (childSnapshot.val().recipient === record.email) {
              let goodToGo = true;
              ignore.forEach((i) => {
                goodToGo = (i !== childSnapshot.val().sender) && goodToGo;
              });
              if (goodToGo) {
                receivedMessages.push(Object.assign(
                  childSnapshot.val(),
                  { body: encrypter.decrypt(childSnapshot.val().body) },
                ));
              }
            }
          });
        })
        .then(() => {
          res.json({ sent: sentMessages, received: receivedMessages });
        });
      });
    })
    .catch((error) => {
      console.log(`getUser Error -> ${error}`);
      res.json({ result: false });
    });
  });
});

router.post('/', (req, res) => {
  const messageRef = firebase.database().ref('/messages/');
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    admin.auth().getUser(uid).then((userRecord) => {
      const record = userRecord.toJSON();
      messageRef.push({
        sender: record.email,
        recipient: req.body.recipient,
        title: req.body.title,
        body: encrypter.encrypt(req.body.body),
        timestamp: Date.now(),
      });
      mailgunClient.messages().send({
        from: 'StoreStuff <mailgun@sandboxde821aab0c504b59be6fe27cbaf151d6.mailgun.org>',
        to: req.body.recipient,
        subject: 'You have a new message on StoreStuff!',
        text: 'Please go to StoreStuff to view your new message.!.',
      }, (error) => {
        if (error) {
          console.log(error);
        }
      });
      res.json({ result: true, message: 'message sent' });
    }).catch((error) => {
      console.log(`getUser Error -> ${error}`);
      res.json({ result: false });
    });
  });
});

module.exports = router;
