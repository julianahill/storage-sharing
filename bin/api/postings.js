/* eslint no-console: 0 */
import express from 'express';
import googlemaps from '../modules/googlemaps';
import { admin } from '../modules/firebase';


const router = express.Router();

router.get('/', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const myPostings = [];
    const postings = [];
    const ignore = [];
    admin.database().ref(`/users/${uid}/blocked`).once('value').then((blocks) => {
      blocks.forEach((block) => {
        ignore.push(block.val());
      });
    })
    .then(() => {
      admin.database().ref('/postings').once('value').then((snapshot) => {
        const promises = [];
        snapshot.forEach((childSnapshot) => {
          if (childSnapshot.val().uid === uid) {
            promises.push(admin.auth().getUser(childSnapshot.val().uid)
              .then((userRecord) => {
                myPostings.push(Object.assign(
                  {},
                  childSnapshot.val(),
                  { id: childSnapshot.key },
                  { ownerName: userRecord.toJSON().email },
                ));
              }));
          } else {
            promises.push(admin.auth().getUser(childSnapshot.val().uid)
              .then((userRecord) => {
                let goodToGo = true;
                ignore.forEach(i => goodToGo = (i !== userRecord.uid) && goodToGo);
                if (goodToGo) {
                  postings.push(Object.assign(
                    {},
                    childSnapshot.val(),
                    { id: childSnapshot.key },
                    { ownerName: userRecord.toJSON().email },
                  ));
                }
              }));
          }
        });
        return Promise.all(promises);
      })
      .then(() => {
        res.json({
          result: true,
          myPostings,
          postings,
        });
      });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
  })
  .catch((error) => {
    console.log(error);
    res.json({ result: false, messge: error.message });
  });
});

router.get('/mypostings', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const postings = [];
    const ignore = [];
    admin.database().ref(`/users/${uid}/blocked`).once('value').then((blocks) => {
      blocks.forEach((block) => {
        ignore.push(block.val());
      });
    })
    .then(() => {
      admin.database().ref('/postings').once('value').then((snapshot) => {
        const promises = [];
        snapshot.forEach((childSnapshot) => {
          if (childSnapshot.val().uid === uid) {
            promises.push(admin.auth().getUser(childSnapshot.val().uid)
              .then((userRecord) => {
                postings.push(Object.assign(
                  {},
                  childSnapshot.val(),
                  { id: childSnapshot.key },
                  { ownerName: userRecord.toJSON().email },
                ));
              }));
          }
        });
        return Promise.all(promises);
      })
      .then(() => {
        res.json({
          result: true,
          postings,
        });
      });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
  })
  .catch((error) => {
    console.log(error);
    res.json({ result: false, messge: error.message });
  });
});

router.get('/:id', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then(() => {
    admin.database().ref(`/postings/${req.params.id}`).once('value').then((snapshot) => {
      admin.auth().getUser(snapshot.val().uid).then((userRecord) => {
        const record = userRecord.toJSON();
        const ownerName = record.email;
        res.json({
          result: true,
          posting: Object.assign(
            {},
            snapshot.val(),
            { id: snapshot.key, ownerName },
          ),
        });
      })
      .catch((error) => {
        console.log(`Get Posting Owner Name Error -> ${error}`);
        res.json({ result: false, message: error.message });
      });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
  })
  .catch((error) => {
    console.log(error);
    res.json({ result: false, messge: error.message });
  });
});

router.post('/create', (req, res) => {
  const token = req.header('Authorization');
  const db = admin.database();
  googlemaps.geocode({
    address: `${req.body.street}, ${req.body.city}, ${req.body.state}`,
  }, (err, resp) => {
    if (err) {
      res.json({ result: false, message: err.message });
    } else {
      admin.auth().verifyIdToken(token).then((decodedToken) => {
        const uid = decodedToken.uid;
        return db.ref(`/users/${uid}`).once('value');
      })
      .then((userSnap) => {
        const user = userSnap.val();
        if (!user.stripeId) {
          throw Error('No stripe connect account');
        }
        const postingsRef = db.ref('/postings');
        const payload = {
          title: req.body.title,
          description: req.body.description,
          images: req.body.images,
          latitude: resp.json.results[0].geometry.location.lat,
          longitude: resp.json.results[0].geometry.location.lng,
          street: req.body.street,
          city: req.body.city,
          state: req.body.state,
          zip: req.body.zip,
          capacity: req.body.capacity,
          rate: req.body.rate,
          frequency: req.body.frequency,
          uid: userSnap.key,
        };
        postingsRef.push().set(payload);
        res.json({ result: true });
      })
      .catch((error) => {
        console.log(`verifyIdToken Error -> ${error}`);
        res.json({ result: false, message: error.message });
      });
    }
  });
});

module.exports = router;
