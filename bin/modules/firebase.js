import * as firebase from 'firebase';
import * as admin from 'firebase-admin';
import * as serviceAccount from '../../firebase-adminsdk.json';

admin.initializeApp({ credential: admin.credential.cert(serviceAccount), databaseURL: 'https://storestuff-84304.firebaseio.com' });

const config = {
  apiKey: 'AIzaSyA0WBa9GF3ewj2oQ3uZ1gnGddyo4ewYPQA',
  authDomain: 'storestuff-84304.firebaseapp.com',
  databaseURL: 'https://storestuff-84304.firebaseio.com',
  storageBucket: 'storestuff-84304.appspot.com',
};

firebase.initializeApp(config);

module.exports = {
  firebase,
  admin,
};
