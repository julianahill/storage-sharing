import stripe from './stripe';

stripe.plans.create({
  amount: 5000,
  interval: 'month',
  name: 'Test Plan 4',
  currency: 'USD',
  id: 'test-plan4',
}, {
  stripe_account: 'acct_1ACyH8EtPlRgHHJy',
})
  .then((plan) => {
    return Promise.all([
      plan,
      stripe.tokens.create({
        customer: 'cus_AY4q5udIrJVWIQ',
      }, {
        stripe_account: 'acct_1ACyH8EtPlRgHHJy',
      }),
    ]);
  })
  .then((results) => {
    const plan = results[0];
    const token = results[1];
    return Promise.all([
      plan,
      stripe.customers.create({
        description: 'Customer for joseph.taylor@example.com',
        source: token.id,
      }, {
        stripe_account: 'acct_1ACyH8EtPlRgHHJy',
      }),
    ]);
  })
  .then((results) => {
    const plan = results[0];
    const customer = results[1];
    return stripe.subscriptions.create({
      customer: customer.id,
      plan: plan.id,
    }, {
      stripe_account: 'acct_1ACyH8EtPlRgHHJy',
    });
  });
