/* eslint no-console: 0 */
import express from 'express';
import bodyParser from 'body-parser';
import rp from 'request-promise';
import _ from 'lodash';
import { apiPort } from '../config/env';
import { firebase, admin } from './modules/firebase';
import stripe from './modules/stripe';
import userRouter from './api/user';
import messagesRouter from './api/messages';
import postingsRouter from './api/postings';
import contractsRouter from './api/contracts';
const app = express();
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  next();
});
app.use(bodyParser.json());
app.get('/api', (req, res) => {
  res.send('Hello World!');
});
app.use('/api/user', userRouter);
app.use('/api/messages', messagesRouter);
app.use('/api/postings', postingsRouter);
app.use('/api/contracts', contractsRouter);
app.post('/api/login', (req, res) => {
  firebase.auth().signInWithEmailAndPassword(req.body.username, req.body.password).then(() => {
    const currentUser = firebase.auth().currentUser;
    currentUser.getToken().then(token =>
      (firebase.database().ref(`/users/${currentUser.uid}`).once('value')
        .then((snapshot) => {

          if (snapshot.val().banned) {
            res.json({ result: false, message: 'banned' });
          } else {
            res.json({
              result: true,
              token,
              user: Object.assign({}, snapshot.val(), { id: snapshot.key }),
            });
          }
        })),
    );
  }).catch((error) => {
    res.json({ result: false, message: error.message });
  });
});

app.get('/api/cards', (req, res) => {
  const token = req.header('Authorization');
  let uid;
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      uid = decodedToken.uid;
      return firebase.database().ref(`/users/${uid}`).once('value');
    })
    .then((snapshot) => {
      const stripeCustId = snapshot.val().stripeCustId;
      if (stripeCustId) {
        return stripe.customers.retrieve(stripeCustId);
      }
      throw new Error('No stripe account found');
    })
    .then((customer) => {
      const card = _.chain(customer)
        .get('sources.data')
        .find(source => source.object === 'card')
        .value();
      if (card) {
        res.json({
          result: true,
          expMonth: card.exp_month,
          expYear: card.exp_year,
          last4: card.last4,
        });
      } else {
        throw new Error('No card found');
      }
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, message: error.message });
    });
});

app.post('/api/cards/create', (req, res) => {
  const token = req.header('Authorization');
  let uid;
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      uid = decodedToken.uid;
      return firebase.database().ref(`/users/${uid}`).once('value');
    })
    .then((snapshot) => {
      const userRecord = snapshot.val();
      if (userRecord.stripeCustId) { // Only update credit card
        return stripe.customers.update(userRecord.stripeCustId, {
          source: {
            object: 'card',
            exp_month: req.body.expMonth,
            exp_year: req.body.expYear,
            number: req.body.cardNumber,
            cvc: req.body.cvc,
          },
        });
      }
      // Create new stripe customer
      return stripe.customers.create({
        email: userRecord.username,
        source: {
          object: 'card',
          exp_month: req.body.expMonth,
          exp_year: req.body.expYear,
          number: req.body.cardNumber,
          cvc: req.body.cvc,
        },
      });
    })
    .then(customer => (
      firebase.database().ref(`/users/${uid}`).update({
        stripeCustId: customer.id,
      })
    ))
    .then(() => {
      res.json({ result: true });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, error: error.message });
    });
});
app.post('/api/hosts/create', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token)
    .then((decodedToken) => {
      const uid = decodedToken.uid;
      return Promise.all([
        uid,
        rp({
          method: 'POST',
          uri: 'https://connect.stripe.com/oauth/token',
          body: {
            grant_type: 'authorization_code',
            client_id: 'ca_ANvqAxVRWcrdGHcHESK6eRvNtYrxLdnp',
            code: req.body.id,
            client_secret: 'sk_test_W0cCszl78cFoCP3BWd1ynd6s',
          },
          json: true, // Automatically stringifies the body to JSON
        }),
      ]);
    })
    .then((results) => {
      const uid = results[0];
      const stripeRes = results[1];
      return firebase.database().ref(`/users/${uid}`).update({
        stripeId: stripeRes.stripe_user_id,
      });
    })
    .then(() => {
      res.json({ result: true });
    })
    .catch((error) => {
      console.log(error);
      res.json({ result: false, error: error.message });
    });
});
app.post('/complaints/create', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const db = admin.database();
    const ref = db.ref('/complaints');
    const payload = {
      posting_title: req.body.posting_title,
      description: req.body.description,
      date: req.body.date,
      uid,
    };
    ref.push().set(payload);
    res.json({ result: true });
  }).catch((error) => {
    console.log(`verifyIdToken Error -> ${error}`);
    res.json({ result: false });
  });
});
app.post('api/bugs/create', (req, res) => {
  const token = req.header('Authorization');
  admin.auth().verifyIdToken(token).then((decodedToken) => {
    const uid = decodedToken.uid;
    const db = admin.database();
    const ref = db.ref('/bugs');
    const payload = {
      description: req.body.description,
      date: req.body.date,
      uid,
    };
    ref.push().set(payload);
    res.json({ result: true });
  }).catch((error) => {
    console.log(`verifyIdToken Error -> ${error}`);
    res.json({ result: false });
  });
});

app.get('/api/req/:rid/accept', (req, res) => {
  const rid = req.params.rid;
  admin.database().ref(`/phoneRequests/${rid}`).once('value')
  .then((snap) => {
    const uid = snap.val().src;
    admin.database().ref(`/users/${snap.val().dest}/phones`).push(uid);
    admin.database().ref(`/phoneRequests/${rid}`).remove();
    res.json({ res: true });
  })
  .catch(() => {
    res.redirect('/notfound');
  });
});

app.listen(apiPort, (err) => {
  if (err) {
    console.error(err);
  } else {
    console.info(`Api listening on port ${apiPort}!`);
  }
});