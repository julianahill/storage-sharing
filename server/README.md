Environment configuration:
Create a file named .env with the same format as .env.example except with the key and domain filled in.

Firebase-Credentials:
1. Navigate to the firebase console for Safekeep
2. Go to permissions next to overview in the side tab
3. Go to service accounts
4. For the Default Service Account click options Create key
5. Save the key as firebase-credentials.json in the safekeep-server folder

