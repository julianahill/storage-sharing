var express = require('express');
var http = require('http');
var firebase = require('firebase');
var twilio = require('twilio');
var dotenv = require('dotenv');
var mailgun = require('mailgun-js');

var app = express();
var server = http.createServer(app);
dotenv.load();

firebase.initializeApp({
  serviceAccount: "firebase-credentials.json",
  databaseURL: "https://safekeep-380e4.firebaseio.com"
});
var rootRef = firebase.database().ref();

var mailgunClient = mailgun({apiKey: process.env.MAILGUN_API_KEY, domain: process.env.MAILGUN_DOMAIN});

var emailsRef = rootRef.child('emails');
emailsRef.on('child_added', function(snapshot){
  var email = snapshot.val();
  mailgunClient.messages().send({
    from: 'Safekeep.club <mailgun@' + process.env.MAILGUN_DOMAIN + '>',
    to: email.emailAddress,
    subject:'Welcome to Safekeep.club!',
    text: 'Thanks for signing up for Safekeep.club!'
  }, function(error, body) {
    console.log(body);
    if(error) {
      console.log(error);
    }
  });
});

server.listen(3030, function() {
  console.log('Listening on http://localhost:3030...');
});
