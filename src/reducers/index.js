import { combineReducers } from 'redux';
import { routerReducer } from 'react-router-redux';
import user from './User';
import postings from './Postings';
import posting from './Posting';
import profile from './Profile';
import contracts from './Contracts';
import contract from './Contract';
import card from './Card';
import createPosting from './CreatePosting';

module.exports = combineReducers({
  routing: routerReducer,
  user,
  postings,
  posting,
  contracts,
  contract,
  card,
  createPosting,
  profile,
});
