import {
  REQUEST_CONTRACT,
  RECEIVE_CONTRACT,
  ERROR_CONTRACT,
  SET_CONTRACT,
  REQUEST_ACCEPT,
  RECEIVE_ACCEPT,
  ERROR_ACCEPT,
} from '../actions/Contract';

const initialState = {
  contract: {},
};

function Contract(state = initialState, action) {
  switch (action.type) {
    case REQUEST_CONTRACT:
      return state;
    case RECEIVE_CONTRACT:
      return Object.assign({}, state, {
        contract: action.json.contract,
      });
    case ERROR_CONTRACT:
      return state;
    case SET_CONTRACT:
      return Object.assign({}, state, {
        contract: action.contract,
      });
    case REQUEST_ACCEPT:
      return state;
    case RECEIVE_ACCEPT:
      return Object.assign({}, state, {
        accepted: action.json.result,
      });
    case ERROR_ACCEPT:
      return state;
    default:
      return state;
  }
}

export default Contract;
