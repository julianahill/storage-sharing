import _ from 'lodash';
import cookie from 'react-cookie';
import cookies from '../utilities/cookies';

import {
  REQUEST_LOGIN,
  RECEIVE_LOGIN,
  ERROR_LOGIN,
  CHECK_LOCAL_STORAGE,
  LOG_OUT,
} from '../actions/Login';

import {
  REQUEST_SIGNUP,
  RECEIVE_SIGNUP,
  ERROR_SIGNUP,
} from '../actions/Signup';

import {
  REQUEST_RESET_PASSWORD,
  RECEIVE_RESET_PASSWORD,
  ERROR_RESET_PASSWORD,
} from '../actions/ResetPassword';

import {
  REQUEST_USER,
  RECEIVE_USER,
  ERROR_USER,
  REQUEST_SAVE_STRIPE,
  RECEIVE_SAVE_STRIPE,
  ERROR_SAVE_STRIPE,
} from '../actions/User';

const initialState = {
  token: null,
  isLoggedIn: false,
  self: {
    id: null,
    username: null,
    stripeCustId: null,
    stripeId: null,
    avatar: null,
    firstName: null,
    lastName: null,
    address: null,
  },
  selectedUser: {
    id: null,
    username: null,
    avatar: null,
    firstName: null,
    lastName: null,
    address: null,
  },
};

function User(state = initialState, action) {
  switch (action.type) {
    case CHECK_LOCAL_STORAGE: {
      const token = cookie.load(cookies.token) || null;
      return Object.assign({}, state, {
        token,
        isLoggedIn: !(token == null),
      });
    }
    case REQUEST_USER:
    case ERROR_USER:
      return state;
    case RECEIVE_USER:
      return Object.assign({}, state, {
        id: action.user.id,
        self: {
          username: action.user.username,
          stripeCustId: action.user.stripeCustId,
          stripeId: action.user.stripeId,
          avatar: action.user.avatar,
          firstName: action.user.firstName,
          lastName: action.user.lastName,
          address: action.user.address,
          phone: action.user.phone,
        },
      });
    case REQUEST_LOGIN:
      return state;
    case RECEIVE_LOGIN:
      cookie.save(cookies.token, action.token);
      cookie.save(cookies.admin, action.user.admin);
      return Object.assign({}, state, {
        token: action.token,
        isLoggedIn: true,
        id: _.get(action.user, 'id'),
        username: _.get(action.user, 'username'),
        admin: _.get(action.user, 'admin'),
      });
    case ERROR_LOGIN:
      return state;
    case REQUEST_SIGNUP:
      return state;
    case RECEIVE_SIGNUP:
      cookie.save(cookies.token, action.token);
      return Object.assign({}, state, {
        token: action.token,
        isLoggedIn: true,
        id: _.get(action.user, 'id'),
        username: _.get(action.user, 'username'),
      });
    case ERROR_SIGNUP:
      return state;
    case REQUEST_RESET_PASSWORD:
      return state;
    case RECEIVE_RESET_PASSWORD:
      return state;
    case ERROR_RESET_PASSWORD:
      return state;
    case LOG_OUT:
      cookie.remove(cookies.token);
      return Object.assign({}, state, {
        token: null,
        isLoggedIn: false,
      });
    case REQUEST_SAVE_STRIPE:
      return state;
    case RECEIVE_SAVE_STRIPE:
      return state;
    case ERROR_SAVE_STRIPE:
      return state;
    default:
      return state;
  }
}

export default User;
