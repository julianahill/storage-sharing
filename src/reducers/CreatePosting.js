import {
  REQUEST_POSTING_CREATE,
  RECEIVE_POSTING_CREATE,
  ERROR_POSTING_CREATE,
} from '../actions/CreatePosting';

const initialState = {
  title: '',
  description: '',
  images: '',
  latitude: '',
  longitude: '',
  capacity: '',
  price: '',
};

function CreatePosting(state = initialState, action) {
  switch (action.type) {
    case REQUEST_POSTING_CREATE:
      return state;
    case RECEIVE_POSTING_CREATE:
      return state;
    case ERROR_POSTING_CREATE:
      return state;
    default:
      return state;
  }
}

export default CreatePosting;
