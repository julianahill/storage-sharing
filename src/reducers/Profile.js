import {
  REQUEST_MY_POSTINGS,
  RECEIVE_MY_POSTINGS,
  ERROR_MY_POSTINGS,
} from '../actions/Profile';

const initialState = {
  myPostings: [],
};

function Profile(state = initialState, action) {
  switch (action.type) {
    case REQUEST_MY_POSTINGS:
      return state;
    case RECEIVE_MY_POSTINGS:
      return Object.assign({}, state, {
        myPostings: action.json.postings,
      });
    case ERROR_MY_POSTINGS:
      return state;
    default:
      return state;
  }
}

export default Profile;
