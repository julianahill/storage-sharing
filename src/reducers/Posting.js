import {
  REQUEST_POSTING,
  RECEIVE_POSTING,
  ERROR_POSTING,
  SET_POSTING,
  REQUEST_CONTRACT,
  RECEIVE_CONTRACT,
  ERROR_CONTRACT,
} from '../actions/Posting';

const initialState = {
  posting: {},
};

function Posting(state = initialState, action) {
  switch (action.type) {
    case REQUEST_POSTING:
      return state;
    case RECEIVE_POSTING:
      return Object.assign({}, state, {
        posting: action.json.posting,
      });
    case ERROR_POSTING:
      return state;
    case SET_POSTING:
      return Object.assign({}, state, {
        posting: action.posting,
      });
    case REQUEST_CONTRACT:
      return state;
    case RECEIVE_CONTRACT:
      return Object.assign({}, state, {
        contract: action.contract,
      });
    case ERROR_CONTRACT:
      return state;
    default:
      return state;
  }
}

export default Posting;
