import {
  REQUEST_CARD,
  RECEIVE_CARD,
  ERROR_CARD,
} from '../actions/Card';

const initialState = {
  expMonth: '',
  expYear: '',
  cardNumber: '',
};

function Card(state = initialState, action) {
  switch (action.type) {
    case REQUEST_CARD:
      return state;
    case RECEIVE_CARD:
      return Object.assign({}, state, {
        expMonth: action.json.expMonth.toString(),
        expYear: action.json.expYear.toString(),
        cardNumber: `************${action.json.last4}`,
      });
    case ERROR_CARD:
      return state;
    default:
      return state;
  }
}

export default Card;
