import {
  REQUEST_CONTRACTS,
  RECEIVE_CONTRACTS,
  ERROR_CONTRACTS,
} from '../actions/Contracts';

const initialState = {
  ownerContracts: [],
  renterContracts: [],
};

function Contracts(state = initialState, action) {
  switch (action.type) {
    case REQUEST_CONTRACTS:
      return state;
    case RECEIVE_CONTRACTS:
      return Object.assign({}, state, {
        ownerContracts: action.json.ownerContracts,
        renterContracts: action.json.renterContracts,
      });
    case ERROR_CONTRACTS:
      return state;
    default:
      return state;
  }
}

export default Contracts;
