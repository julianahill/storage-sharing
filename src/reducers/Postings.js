import {
  REQUEST_POSTINGS,
  RECEIVE_POSTINGS,
  ERROR_POSTINGS,
} from '../actions/Postings';

const initialState = {
  publicPostings: [],
  myPostings: [],
};

function Postings(state = initialState, action) {
  switch (action.type) {
    case REQUEST_POSTINGS:
      return state;
    case RECEIVE_POSTINGS:
      return Object.assign({}, state, {
        publicPostings: action.json.postings,
        myPostings: action.json.myPostings,
      });
    case ERROR_POSTINGS:
      return state;
    default:
      return state;
  }
}

export default Postings;
