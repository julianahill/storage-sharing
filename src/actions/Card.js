import fetch from 'isomorphic-fetch';

export const REQUEST_CARD = 'REQUEST_CARD';
function requestCard() {
  return {
    type: REQUEST_CARD,
  };
}

export const RECEIVE_CARD = 'RECEIVE_CARD';
function receiveCard(json) {
  return {
    type: RECEIVE_CARD,
    json,
  };
}

export const ERROR_CARD = 'ERROR_CARD';
function errorCard(error) {
  return {
    type: ERROR_CARD,
    error,
  };
}

export function loadCard(token) {
  return (dispatch) => {
    dispatch(requestCard());
    return fetch('/api/cards', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        return dispatch(receiveCard(json));
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorCard(e)));
  };
}
