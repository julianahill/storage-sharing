import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';

export const REQUEST_POSTING_CREATE = 'REQUEST_POSTING_CREATE';
function requestPostingCreate(formData) {
  return {
    type: REQUEST_POSTING_CREATE,
    formData,
  };
}

export const RECEIVE_POSTING_CREATE = 'RECEIVE_POSTING_CREATE';
function receivePostingCreate() {
  return {
    type: RECEIVE_POSTING_CREATE,
  };
}

export const ERROR_POSTING_CREATE = 'ERROR_POSTING_CREATE';
function errorPostingCreate(error) {
  return {
    type: ERROR_POSTING_CREATE,
    error,
  };
}

export function createPosting(token, formData) {
  return (dispatch) => {
    dispatch(requestPostingCreate(formData));
    return fetch('/api/postings/create', {
      method: 'POST',
      headers: {
        Authorization: token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        dispatch(receivePostingCreate());
        browserHistory.push('/postings');
        return json;
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorPostingCreate(e)));
  };
}
