import fetch from 'isomorphic-fetch';

export const REQUEST_COMPLAINT= 'REQUEST_COMPLAINT';
function requestComplaint(formData) {
        return {
                type: REQUEST_COMPLAINT,
                 formData,
        };
}

export const RECEIVE_COMPLAINT = 'RECEIVE_COMPLAINT';
        function receiveComplaint(json) {
         return {
                type: RECEIVE_COMPLAINT,
                token: json.token,
        };
}

export const ERROR_COMPLAINT = 'ERROR_COMPLAINT';
function errorComplaint(error) {
  return {
    type: ERROR_COMPLAINT,
    error,
  };
}


export function createComplaintformData) {
        return (dispatch) => {
                dispatch(requestComplaint(formData));
                return fetch('/api/complaints/create', {
                        method: 'POST',
                        headers: {
                                Accept: 'application/json',
                                'Content-Type': 'application/json',
                        },
                body: JSON.stringify(formData),
        })
         .then(response => response.json())
      .then((json) => {
        if (json.result) {
          return dispatch(receiveComplaint(json));
        }
       throw new Error(json.message);
      })
      .catch(e => dispatch(errorComplaint(e)));
  };
}


