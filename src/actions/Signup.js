import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';

export const REQUEST_SIGNUP = 'REQUEST_SIGNUP';
function requestSignup(formData) {
  return {
    type: REQUEST_SIGNUP,
    formData,
  };
}

export const RECEIVE_SIGNUP = 'RECEIVE_SIGNUP';
function receiveSignup(json) {
  return {
    type: RECEIVE_SIGNUP,
    token: json.token,
    user: json.user,
  };
}

export const ERROR_SIGNUP = 'ERROR_SIGNUP';
function errorSignup(error) {
  return {
    type: ERROR_SIGNUP,
    error,
  };
}

export function signup(formData) {
  return (dispatch) => {
    dispatch(requestSignup(formData));
    return fetch('/api/user/create', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          dispatch(receiveSignup(json));
          return json;
        }
        throw new Error(json.message);
      })
      .then((json) => {
        if (json.result) {
          browserHistory.push('/');
        }
      })
      .catch(e => dispatch(errorSignup(e)));
  };
}
