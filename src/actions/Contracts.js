import fetch from 'isomorphic-fetch';

export const REQUEST_CONTRACTS = 'REQUEST_CONTRACTS';
function requestContracts() {
  return {
    type: REQUEST_CONTRACTS,
  };
}

export const RECEIVE_CONTRACTS = 'RECEIVE_CONTRACTS';
function receiveContracts(json) {
  return {
    type: RECEIVE_CONTRACTS,
    json,
  };
}

export const ERROR_CONTRACTS = 'ERROR_CONTRACTS';
function errorContracts(error) {
  return {
    type: ERROR_CONTRACTS,
    error,
  };
}

export function load(token) {
  return (dispatch) => {
    dispatch(requestContracts());
    return fetch('/api/contracts', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          return dispatch(receiveContracts(json));
        }
        throw new Error(json.message);
      })
      .catch(e => dispatch(errorContracts(e)));
  };
}
