import fetch from 'isomorphic-fetch';

export const REQUEST_MY_POSTINGS = 'REQUEST_MY_POSTINGS';
function requestMyPostings() {
  return {
    type: REQUEST_MY_POSTINGS,
  };
}

export const RECEIVE_MY_POSTINGS = 'RECEIVE_MY_POSTINGS';
function receiveMyPostings(json) {
  return {
    type: RECEIVE_MY_POSTINGS,
    json,
  };
}

export const ERROR_MY_POSTINGS = 'ERROR_MY_POSTINGS';
function errorMyPostings(error) {
  return {
    type: ERROR_MY_POSTINGS,
    error,
  };
}

export function load(token) {
  return (dispatch) => {
    dispatch(requestMyPostings());
    return fetch('/api/postings/mypostings', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        return dispatch(receiveMyPostings(json));
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorMyPostings(e)));
  };
}
