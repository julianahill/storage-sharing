import fetch from 'isomorphic-fetch';

export const REQUEST_RESET_PASSWORD = 'REQUEST_RESET_PASSWORD';
function requestResetPassword(formData) {
  return {
    type: REQUEST_RESET_PASSWORD,
    formData,
  };
}

export const RECEIVE_RESET_PASSWORD = 'RECEIVE_RESET_PASSWORD';
function receiveResetPassword(json) {
  return {
    type: RECEIVE_RESET_PASSWORD,
    token: json.token,
  };
}

export const ERROR_RESET_PASSWORD = 'ERROR_RESET_PASSWORD';
function errorResetPassword(error) {
  return {
    type: ERROR_RESET_PASSWORD,
    error,
  };
}

export function resetPassword(formData) {
  return (dispatch) => {
    dispatch(requestResetPassword(formData));
    return fetch('/api/user/resetpassword', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          return dispatch(receiveResetPassword(json));
        }
        throw new Error(json.message);
      })
      .catch(e => dispatch(errorResetPassword(e)));
  };
}
