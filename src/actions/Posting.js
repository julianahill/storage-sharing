import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';

export const REQUEST_POSTING_CREATE = 'REQUEST_POSTING_CREATE';
function requestPostingCreate(formData) {
  return {
    type: REQUEST_POSTING_CREATE,
    formData,
  };
}

export const RECEIVE_POSTING_CREATE = 'RECEIVE_POSTING_CREATE';
function receivePostingCreate(json) {
  return {
    type: RECEIVE_POSTING_CREATE,
    token: json.token,
  };
}

export const ERROR_POSTING_CREATE = 'ERROR_POSTING_CREATE';
function errorPostingCreate(error) {
  return {
    type: ERROR_POSTING_CREATE,
    error,
  };
}

export function createPosting(token, formData) {
  return (dispatch) => {
    dispatch(requestPostingCreate(formData));
    return fetch('/api/postings/create', {
      method: 'POST',
      headers: {
        Authorization: token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        return dispatch(receivePostingCreate(json));
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorPostingCreate(e)));
  };
}

export const REQUEST_POSTING = 'REQUEST_POSTING';
function requestPosting() {
  return {
    type: REQUEST_POSTING,
  };
}

export const RECEIVE_POSTING = 'RECEIVE_POSTING';
function receivePosting(json) {
  return {
    type: RECEIVE_POSTING,
    json,
  };
}

export const ERROR_POSTING = 'ERROR_POSTING';
function errorPosting(error) {
  return {
    type: ERROR_POSTING,
    error,
  };
}

export const SET_POSTING = 'SET_POSTING';
export function setPosting(posting) {
  return {
    type: SET_POSTING,
    posting,
  };
}

export function getPosting(token, postingId) {
  return (dispatch) => {
    dispatch(requestPosting());
    return fetch(`/api/postings/${postingId}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        return dispatch(receivePosting(json));
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorPosting(e)));
  };
}

export const REQUEST_CONTRACT = 'REQUEST_CONTRACT';
function requestContract(formData) {
  return {
    type: REQUEST_CONTRACT,
    formData,
  };
}

export const RECEIVE_CONTRACT = 'RECEIVE_CONTRACT';
function receiveContract(json) {
  return {
    type: RECEIVE_CONTRACT,
    json,
  };
}

export const ERROR_CONTRACT = 'ERROR_CONTRACT';
function errorContract(error) {
  return {
    type: ERROR_CONTRACT,
    error,
  };
}

export function createContract(token, formData) {
  return (dispatch) => {
    dispatch(requestContract(formData));
    return fetch('/api/contracts/create', {
      method: 'POST',
      headers: {
        Authorization: token,
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        dispatch(receiveContract(json));
        browserHistory.push('/contracts');
        return json;
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorContract(e)));
  };
}
