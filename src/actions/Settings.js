import fetch from 'isomorphic-fetch';

export const REQUEST_SAVE_CARD = 'REQUEST_SAVE_CARD';
function requestSaveCard(formData) {
  return {
    type: REQUEST_SAVE_CARD,
    formData,
  };
}

export const RECEIVE_SAVE_CARD = 'RECEIVE_SAVE_CARD';
function receiveSaveCard(json) {
  return {
    type: RECEIVE_SAVE_CARD,
    token: json.token,
    user: json.user,
  };
}

export const ERROR_SAVE_CARD = 'ERROR_SAVE_CARD';
function errorSaveCard(error) {
  return {
    type: ERROR_SAVE_CARD,
    error,
  };
}

export function saveCreditCard(card, token) {
  return (dispatch) => {
    dispatch(requestSaveCard());
    return fetch('/api/cards/create', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(card),
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          return dispatch(receiveSaveCard(json));
        }
        throw new Error(json.message);
      })
      .catch(e => dispatch(errorSaveCard(e)));
  };
}

export const REQUEST_SAVE_DETAILS = 'REQUEST_SAVE_DETAILS';
function requestSaveDetails(formData) {
  return {
    type: REQUEST_SAVE_DETAILS,
    formData,
  };
}

export const RECEIVE_SAVE_DETAILS = 'RECEIVE_SAVE_DETAILS';
function receiveSaveDetails(json) {
  return {
    type: RECEIVE_SAVE_DETAILS,
    token: json.token,
    user: json.user,
  };
}

export const ERROR_SAVE_DETAILS = 'ERROR_SAVE_DETAILS';
function errorSaveDetails(error) {
  return {
    type: ERROR_SAVE_DETAILS,
    error,
  };
}

export function savePersonalDetail(details, token) {
  return (dispatch) => {
    dispatch(requestSaveDetails());
    return fetch('/api/user', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        Authorization: token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(details),
    })
      .then(response => response.json())
      .then((response) => {
        if (response.result) {
          return dispatch(receiveSaveDetails(response));
        }
        throw new Error(response.message);
      })
      .catch(e => dispatch(errorSaveDetails(e)));
  };
}
