import fetch from 'isomorphic-fetch';

export const REQUEST_POSTINGS = 'REQUEST_POSTINGS';
function requestPostings() {
  return {
    type: REQUEST_POSTINGS,
  };
}

export const RECEIVE_POSTINGS = 'RECEIVE_POSTINGS';
function receivePostings(json) {
  return {
    type: RECEIVE_POSTINGS,
    json,
  };
}

export const ERROR_POSTINGS = 'ERROR_POSTINGS';
function errorPostings(error) {
  return {
    type: ERROR_POSTINGS,
    error,
  };
}

export function load(token) {
  return (dispatch) => {
    dispatch(requestPostings());
    return fetch('/api/postings', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          return dispatch(receivePostings(json));
        }
        throw new Error(json.message);
      })
      .catch(e => dispatch(errorPostings(e)));
  };
}
