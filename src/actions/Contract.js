import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';

export const REQUEST_CONTRACT = 'REQUEST_CONTRACT';
function requestContract() {
  return {
    type: REQUEST_CONTRACT,
  };
}

export const RECEIVE_CONTRACT = 'RECEIVE_CONTRACT';
function receiveContract(json) {
  return {
    type: RECEIVE_CONTRACT,
    json,
  };
}

export const ERROR_CONTRACT = 'ERROR_CONTRACT';
function errorContract(error) {
  return {
    type: ERROR_CONTRACT,
    error,
  };
}

export const SET_CONTRACT = 'SET_CONTRACT';
export function setContract(contract) {
  return {
    type: SET_CONTRACT,
    contract,
  };
}

export function getContract(token, contractId) {
  return (dispatch) => {
    dispatch(requestContract());
    return fetch(`/api/contracts/${contractId}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        dispatch(receiveContract(json));
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorContract(e)));
  };
}

export const REQUEST_ACCEPT = 'REQUEST_ACCEPT';
function requestAccept() {
  return {
    type: REQUEST_ACCEPT,
  };
}

export const RECEIVE_ACCEPT = 'RECEIVE_ACCEPT';
function receiveAccept(json) {
  return {
    type: RECEIVE_ACCEPT,
    json,
  };
}

export const ERROR_ACCEPT = 'ERROR_ACCEPT';
function errorAccept(error) {
  return {
    type: ERROR_ACCEPT,
    error,
  };
}

export function acceptContract(token, contractId) {
  return (dispatch) => {
    dispatch(requestAccept());
    return fetch(`/api/contracts/${contractId}/accept`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
      },
    })
    .then(response => response.json())
    .then((json) => {
      if (json.result) {
        dispatch(receiveAccept(json));
        browserHistory.push('/contracts');
      }
      throw new Error(json.message);
    })
    .catch(e => dispatch(errorAccept(e)));
  };
}
