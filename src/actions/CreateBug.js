import React from 'react';
import fetch from 'isomorphic-fetch';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';
import cookies from '../utilities/cookies';


export default class CreateBug extends React.Component {
  // https://facebook.github.io/react/docs/forms.html

  constructor(props) {
    super(props);

    this.state = {
      status: 'No bug filed',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handlePosting = this.handlePosting.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleBug(event) {
    event.preventDefault();
    const formData = {
      description: this.state.description,
      date: this.state.date,
    };

    return fetch('api/bugs/create', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    }).then(response => response.json().then((json) => {
      if (json.result) {
        this.setState({ status: json.token });
        cookie.save(cookies.token, json.token);
        browserHistory.push('/');
      } else {
        this.setState({ status: 'Please try again!' });
      }
    }));
  }


  render() {
    return (
      <form role="form" onSubmit={this.handleBug}>
        <h3>CreateComplaint</h3>
        <div className="form-group">
          <label htmlFor="description">description</label>
          <input
            type="text"
            name="description"
            onChange={this.handleInputChange}
            value={this.state.description}
          />
        </div>
        <div className="form-group">
          <label htmlFor="date">date</label>
          <input
            type="text"
            name="date"
            onChange={this.handleInputChange}
            value={this.state.date}
          />
        </div>

        <button type="submit">CreateBug</button>
      </form>
    );
  }
}

