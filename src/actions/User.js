import fetch from 'isomorphic-fetch';
import { push } from 'react-router-redux';

export const REQUEST_SAVE_STRIPE = 'REQUEST_SAVE_STRIPE';
function requestSaveStripe() {
  return {
    type: REQUEST_SAVE_STRIPE,
  };
}

export const RECEIVE_SAVE_STRIPE = 'RECEIVE_SAVE_STRIPE';
function receiveSaveStripe(json) {
  return {
    type: RECEIVE_SAVE_STRIPE,
    user: json.user,
    token: json.token,
  };
}

export const ERROR_SAVE_STRIPE = 'ERROR_SAVE_STRIPE';
function errorSaveStripe(error) {
  return {
    type: ERROR_SAVE_STRIPE,
    error,
  };
}

export function saveStripeAccount(id, token) {
  return (dispatch) => {
    dispatch(push('/host'));
    dispatch(requestSaveStripe());
    return fetch('/api/hosts/create', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: token,
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ id }),
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          dispatch(receiveSaveStripe(json));
          dispatch(push('/postings/create'));
          return;
        }
        throw new Error(json.message);
      })
      .catch(e => dispatch(errorSaveStripe(e)));
  };
}

export const REQUEST_USER = 'REQUEST_USER';
function requestUser() {
  return {
    type: REQUEST_USER,
  };
}

export const RECEIVE_USER = 'RECEIVE_USER';
function receiveUser(json) {
  return {
    type: RECEIVE_USER,
    user: json.user,
  };
}

export const ERROR_USER = 'ERROR_USER';
function errorUser(error) {
  return {
    type: ERROR_USER,
    error,
  };
}

export function loadUser(token) {
  return (dispatch) => {
    dispatch(requestUser());
    return fetch('/api/user', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: token,
        'Content-Type': 'application/json',
      },
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          dispatch(receiveUser(json));
          return;
        }
        throw new Error(json.message);
      })
      .catch(e => dispatch(errorUser(e)));
  };
}
