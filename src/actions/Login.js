import fetch from 'isomorphic-fetch';
import { browserHistory } from 'react-router';

export const REQUEST_LOGIN = 'REQUEST_LOGIN';
function requestLogin(formData) {
  return {
    type: REQUEST_LOGIN,
    formData,
  };
}

export const RECEIVE_LOGIN = 'RECEIVE_LOGIN';
function receiveLogin(json) {
  return {
    type: RECEIVE_LOGIN,
    user: json.user,
    token: json.token,
    admin: json.admin,
  };
}

export const ERROR_LOGIN = 'ERROR_LOGIN';
function errorLogin(error) {
  // TODO display banned notification
  return {
    type: ERROR_LOGIN,
    error,
  };
}

export const CHECK_LOCAL_STORAGE = 'CHECK_LOCAL_STORAGE';
export function checkLocalStorage() {
  return {
    type: CHECK_LOCAL_STORAGE,
  };
}

export const LOG_OUT = 'LOG_OUT';
export function logOut() {
  return {
    type: LOG_OUT,
  };
}

export function login(formData) {
  return (dispatch) => {
    dispatch(requestLogin(formData));
    return fetch('/api/login', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    })
      .then(response => response.json())
      .then((json) => {
        if (json.result) {
          dispatch(receiveLogin(json));
          return json;
        }
        throw new Error(json.message);
      })
      .then((json) => {
        if (json.hasTempPassword) {
          browserHistory.push('/information');
        } else if (json.result) {
          browserHistory.push('/');
        }
      })
      .catch(e => dispatch(errorLogin(e)));
  };
}
