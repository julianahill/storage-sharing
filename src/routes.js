/* eslint react/jsx-filename-extension: 0 */

import React from 'react';
import { IndexRoute, Route } from 'react-router';
import _ from 'lodash';
import { checkLocalStorage } from './actions/Login';
import { loadUser } from './actions/User';
import {
  App,
  Landing,
  Login,
  Information,
  Profile,
  OtherInformation,
  ResetPassword,
  Signup,
  NotFound,
  CreatePosting,
  CreateComplaint,
  CreateBug,
  CreateStripeAccount,
  Messages,
  MessageComposer,
  Postings,
  Posting,
  Contracts,
  Contract,
  Settings,
} from './containers';

export default (store) => { // eslint-disable-line
  store.dispatch(checkLocalStorage());

  function checkAuth(state, replaceWith) {
    const isLoggedIn = _.get(store.getState(), 'user.isLoggedIn');
    if (!isLoggedIn) {
      replaceWith('/login');
    } else {
      store.dispatch(loadUser(_.get(store.getState(), 'user.token')));
    }
  }

  return (
    <Route path="/" component={App}>
      { /* Home (main) route */ }
      <IndexRoute component={Landing} />
      <Route path="/login" component={Login} />
      <Route path="/signup" component={Signup} />
      <Route path="/resetpassword" component={ResetPassword} />
      <Route path="/profile" onEnter={checkAuth} component={Profile} />
      <Route path="/profile/:uid" onEnter={checkAuth} component={Profile} />
      <Route path="/information" onEnter={checkAuth} component={Information} />
      <Route path="/complaint" component={CreateComplaint} />
      <Route path="/information/:uid" component={OtherInformation} />
      <Route path="/messages" component={Messages} />
      <Route path="/sendmessage" component={MessageComposer} />
      <Route path="/host" component={CreateStripeAccount} />
      <Route
        path="/postings/create"
        onEnter={(state, replaceWith) => {
          checkAuth(state, replaceWith);
          const hasConnectAccount = _.get(store.getState(), 'user.self.stripeId');
          if (!hasConnectAccount) {
            replaceWith('/host');
          }
        }}
        component={CreatePosting}
      />
      <Route path="/postings" onEnter={checkAuth} component={Postings} />
      <Route path="/postings/:postingId" onEnter={checkAuth} component={Posting} />
      <Route path="/contracts" onEnter={checkAuth} component={Contracts} />
      <Route path="/contracts/:contractId" onEnter={checkAuth} component={Contract} />
      <Route path="/bugs" component={CreateBug} />
      <Route path="/settings" onEnter={checkAuth} component={Settings} />
      { /* Catch all route */ }
      <Route path="*" component={NotFound} status={404} />
    </Route>
  );
};
