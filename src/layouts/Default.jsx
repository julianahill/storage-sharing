/* eslint react/prefer-stateless-function: 0, react/no-danger: 0 */
/* eslint react/forbid-prop-types: 0, react/no-array-index-key: 0 */
/* eslint no-underscore-dangle: 0, global-require: 0 */
import React from 'react';
import ReactDOM from 'react-dom/server';
import serialize from 'serialize-javascript';

export default class Default extends React.Component {
  render() {
    const { assets, component, store } = this.props;
    const content = component ? ReactDOM.renderToString(component) : '';
    return (
      <html lang="en">
        <head>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAMJxRE3SylAwPk1lokVv14rqFXis08Gwo&libraries=places" />
          <title>StoreStuff.Club</title>
          {/* production */}
          {Object.keys(assets.styles).map((style, key) =>
            <link
              href={assets.styles[style]}
              key={key} media="screen, projection"
              rel="stylesheet" type="text/css" charSet="UTF-8"
            />,
          )}
          {/* development */}
          {
            Object.keys(assets.styles).length === 0 ?
              <style dangerouslySetInnerHTML={{ __html: require('../styles/App.css')._style }} /> :
            null
          }
        </head>
        <body>
          <div id="root" dangerouslySetInnerHTML={{ __html: content }} />
          <script
            dangerouslySetInnerHTML={{ __html: `window.__data=${serialize(store.getState())};` }}
            charSet="UTF-8"
          />
          <script
            src={assets.javascript.main}
            charSet="UTF-8"
          />
        </body>
      </html>
    );
  }
}

Default.propTypes = {
  assets: React.PropTypes.object.isRequired,
  component: React.PropTypes.node.isRequired,
  store: React.PropTypes.object.isRequired,
};
