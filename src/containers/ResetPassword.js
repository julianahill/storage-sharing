import { connect } from 'react-redux';
import { resetPassword } from '../actions/ResetPassword';
import { ResetPassword } from '../components/';

const mapStateToProps = () => ({ test: 'test' });

const mapDispatchToProps = dispatch => ({
  onSubmit: formData => dispatch(resetPassword(formData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ResetPassword);
