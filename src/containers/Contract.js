import { connect } from 'react-redux';
import Contract from '../components/Contract';
import { getContract, setContract, acceptContract } from '../actions/Contract';

const mapStateToProps = state => ({
  ownerContracts: state.contracts.ownerContracts,
  renterContracts: state.contracts.renterContracts,
  contract: state.contract.contract,
  token: state.user.token,
  uid: state.user.id,
});

const mapDispatchToProps = dispatch => ({
  onGetContract: (token, contractId) => dispatch(getContract(token, contractId)),
  onSetContract: contract => dispatch(setContract(contract)),
  onAcceptContract: (token, contractId) => dispatch(acceptContract(token, contractId)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contract);
