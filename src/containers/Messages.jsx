/* eslint jsx-a11y/no-static-element-interactions: 0 */
import React from 'react';
import * as firebase from 'firebase';
import fetch from 'isomorphic-fetch';
import classnames from 'classnames';
import cookie from 'react-cookie';
import { Link } from 'react-router';
import cookies from '../utilities/cookies';
import styles from '../styles/Messages.css';

const renderMessages = messages => (
  messages.map(message => (
    <Message {...message} />
  ))
);

class Message extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
    };
  }

  renderBody() {
    if (this.state.open) {
      return (
        <div className={styles.messageBody}>
          {this.props.body}
        </div>
      );
    }
    return null;
  }

  render() {
    return (
      <div
        className={styles.message}
        onClick={() => this.setState({ open: !this.state.open })}
      >
        <div>
          <strong>From: </strong>
          {this.props.sender}
        </div>
        <div>
          <strong>Subject: </strong>
          {this.props.title}
        </div>
        {this.renderBody()}
      </div>
    );
  }
}

Message.propTypes = {
  sender: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
  body: React.PropTypes.string.isRequired,
};

export default class Messages extends React.Component {
  constructor() {
    super();
    const config = {
      apiKey: 'AIzaSyA0WBa9GF3ewj2oQ3uZ1gnGddyo4ewYPQA',
      authDomain: 'storestuff-84304.firebaseapp.com',
      databaseURL: 'https://storestuff-84304.firebaseio.com',
      storageBucket: 'storestuff-84304.appspot.com',
    };
    if (firebase.apps.length === 0) {
      firebase.initializeApp(config);
    }
    this.state = {
      receivedMessages: [],
      sentMessages: [],
    };
  }

  componentDidMount() {
    fetch('/api/messages', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
      },
    }).then(response => response.json().then((json) => {
      if (json.sent) {
        this.setState({
          sentMessages: json.sent,
        });
        // console.log(`${this.state.sentMessages.length}`);
      }
      if (json.received) {
        this.setState({
          receivedMessages: json.received,
        });
        // console.log(`${this.state.receivedMessages.length}`);
      }
    }));
  }

  render() {
    return (
      <div>
        <div className={classnames('container', styles.container)}>
          <div className={classnames(styles.column, styles.leftColumn)}>
            <h3>Sent Messages</h3>
            {renderMessages(this.state.sentMessages)}
          </div>
          <div className={classnames(styles.column, styles.rightColumn)}>
            <h3>Received Messages</h3>
            {renderMessages(this.state.receivedMessages)}
          </div>
        </div>
        <br/>
        <Link className={classnames(styles.button, styles.link)} to="/sendmessage">Send a Message</Link>
    </div>
    );
  }
}
