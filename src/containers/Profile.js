import { connect } from 'react-redux';
import Profile from '../components/Profile';
import { load } from '../actions/Profile';

const mapStateToProps = state => ({
  token: state.user.token,
  firstName: state.user.self.firstName,
  lastName: state.user.self.lastName,
  avatar: state.user.self.avatar,
  address: state.user.self.address,
  username: state.user.self.username,
  phone: state.user.self.phone,
  myPostings: state.profile.myPostings,
});

const mapDispatchToProps = dispatch => ({
  onLoad: token => dispatch(load(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Profile);
