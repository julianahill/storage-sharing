import { connect } from 'react-redux';
import CreatePosting from '../components/CreatePosting';
import { createPosting } from '../actions/CreatePosting';

const mapStateToProps = state => ({
  token: state.user.token,
});

const mapDispatchToProps = dispatch => ({
  onCreatePosting: (token, formData) => dispatch(createPosting(token, formData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CreatePosting);
