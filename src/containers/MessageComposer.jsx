import React from 'react';
import * as firebase from 'firebase';
import fetch from 'isomorphic-fetch';
import cookie from 'react-cookie';
import cookies from '../utilities/cookies';
import styles from '../styles/CreditCardForm.css';

export default class MessageComposer extends React.Component {
  constructor() {
    super();
    const config = {
      apiKey: 'AIzaSyA0WBa9GF3ewj2oQ3uZ1gnGddyo4ewYPQA',
      authDomain: 'storestuff-84304.firebaseapp.com',
      databaseURL: 'https://storestuff-84304.firebaseio.com',
      storageBucket: 'storestuff-84304.appspot.com',
    };
    if (firebase.apps.length === 0) {
      firebase.initializeApp(config);
    }
    this.state = {
      recipient: '',
      title: '',
      body: '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSendMessage = this.handleSendMessage.bind(this);
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleSendMessage(event) {
    event.preventDefault();
    const formData = {
      recipient: this.state.recipient,
      title: this.state.title,
      body: this.state.body,
    };
    console.log(JSON.stringify(formData));
    return fetch('/api/messages', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: cookie.load(cookies.token),
      },
      body: JSON.stringify(formData),
    });
  }

  render() {
    return (
      <div className="jumbotron">
        <form className={styles.form} role="form" onSubmit={this.handleSendMessage}>
          <h1 className={styles.title}>Compose Message</h1>
          <div className="input-group">
            <label className="input-group-addon" id="basic-addon1">To</label>
            <input
              type="text"
              className="form-control"
              placeholder="Send To"
              name="recipient"
              onChange={this.handleInputChange}
              value={this.state.recipient}
            />
          </div>
          <div className="input-group">
            <label className="input-group-addon" id="basic-addon1">Subject</label>
            <input
              type="text"
              className="form-control"
              name="title"
              onChange={this.handleInputChange}
              value={this.state.title}
            />
          </div>
          <div className="input-group">
            <label className="input-group-addon" id="basic-addon1">Message</label>
            <textarea
              type="text"
              className="form-control"
              name="body"
              onChange={this.handleInputChange}
              value={this.state.body}
            ></textarea>
          </div>
          <br />
          <button type="submit">Send</button>
        </form>
      </div>
    );
  }
}
