/* eslint react/prefer-stateless-function: 0, react/forbid-prop-types: 0 */
import React from 'react';
import * as firebase from 'firebase';
import { connect } from 'react-redux';
import { Navbar, LandingNavbar } from '../components';
import { checkLocalStorage, logOut } from '../actions/Login';

import styles from '../styles/App.css';

class App extends React.Component {
  constructor() {
    super();
    const config = {
      apiKey: 'AIzaSyA0WBa9GF3ewj2oQ3uZ1gnGddyo4ewYPQA',
      authDomain: 'storestuff-84304.firebaseapp.com',
      databaseURL: 'https://storestuff-84304.firebaseio.com',
      storageBucket: 'storestuff-84304.appspot.com',
    };
    if (firebase.apps.length === 0) {
      firebase.initializeApp(config);
    }
  }

  render() {
    if (this.props.location.pathname === '/') {
      return (
        <div className={styles.app}>
          <LandingNavbar
            logOut={this.props.logOut}
            isLoggedIn={this.props.isLoggedIn}
          />
          {this.props.children}
        </div>
      );
    }

    return (
      <div className={styles.app}>
        <Navbar
          logOut={this.props.logOut}
          isLoggedIn={this.props.isLoggedIn}
        />
        {this.props.children}
      </div>
    );
  }
}

App.propTypes = {
  location: React.PropTypes.shape({
    pathname: React.PropTypes.string.isRequired,
  }).isRequired,
  children: React.PropTypes.any.isRequired,
  logOut: React.PropTypes.func.isRequired,
  isLoggedIn: React.PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isLoggedIn: state.user.isLoggedIn,
});

const mapDispatchToProps = dispatch => ({
  checkLocalStorage: () => dispatch(checkLocalStorage()),
  logOut: () => dispatch(logOut()),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
