/* eslint react/prefer-stateless-function: 0 */
import React from 'react';
import fetch from 'isomorphic-fetch';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';
import cookies from '../utilities/cookies';
import styles from '../styles/CreditCardForm.css';

export default class CreateComplaint extends React.Component {
  // https://facebook.github.io/react/docs/forms.html

  constructor(props) {
    super(props);

    this.state = { status: 'No complaint made' };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleComplaint = this.handleComplaint.bind(this);
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({ [name]: value });
  }

  handleComplaint(event) {
    event.preventDefault();
    const formData = {
      posting_title: this.state.posting_title,
      description: this.state.description,
      date: this.state.date,
    };

    return fetch('api/complaints/create', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(formData),
    }).then(response => response.json().then((json) => {
      if (json.result) {
        this.setState({ status: json.token });
        cookie.save(cookies.token, json.token);
        browserHistory.push('/');
      } else {
        this.setState({ status: 'Please try again!' });
      }
    }));
  }

  render() {
    return (
      <form className={styles.form} role="form" onSubmit={this.handleComplaint}>
        <h1 className={styles.title}>Create Complaint</h1>
        <br/>
        <div className="form-group">
          <label htmlFor="posting_title">Posting Title</label>
          <input
            type="text"
            name="posting_title"
            onChange={this.handleInputChange}
            value={this.state.posting_title}
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            type="text"
            name="description"
            onChange={this.handleInputChange}
            value={this.state.description}
          />
        </div>
        <div className="form-group">
          <label htmlFor="date">Date</label>
          <input
            type="text"
            name="date"
            onChange={this.handleInputChange}
            value={this.state.date}
          />
        </div>

        <button type="submit">CreateComplaint</button>
      </form>
    );
  }
}
