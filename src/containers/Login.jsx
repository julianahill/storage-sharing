/* eslint react/prefer-stateless-function: 0 */
import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import { login } from '../actions/Login';
import styles from '../styles/Form.css';

class Login extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      pass: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  handleLogin(event) {
    event.preventDefault();
    const formData = {
      username: this.state.user,
      password: this.state.pass,
    };
    this.props.onSubmit(formData);
  }

  render() {
    return (
      <div className={styles.container}>
        <h1 className={styles.title}>Account Login</h1>
        <form className={styles.card} role="form" onSubmit={this.handleLogin}>
          <div>
            <label className={styles.label} htmlFor="user">Email Address</label>
            <input
              className={styles.input}
              type="text"
              placeholder="sjobs@apple.com"
              name="user"
              onChange={this.handleInputChange}
              value={this.state.user}
            />
          </div>
          <div>
            <label className={styles.label} htmlFor="pass">Password</label>
            <input
              className={styles.input}
              type="password"
              placeholder="tinkerbell123"
              name="pass"
              onChange={this.handleInputChange}
              value={this.state.pass}
            />
          </div>
          <div className={styles.buttonContainer}>
            <div />
            <button className={styles.button} type="submit">Log In</button>
          </div>
        </form>
        <Link className={styles.link} to="/resetpassword">Trouble logging in?</Link>
      </div>
    );
  }
}

Login.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
};


const mapStateToProps = state => ({ token: state.user.token });

const mapDispatchToProps = dispatch => ({ onSubmit: formData => dispatch(login(formData)) });

export default connect(mapStateToProps, mapDispatchToProps)(Login);
