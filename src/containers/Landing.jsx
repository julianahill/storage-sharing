/* eslint react/prefer-stateless-function: 0 */
import React from 'react';

import styles from '../styles/Landing.css';

export default class Landing extends React.Component {
  render() {
    return (
      <div className={styles.landingImage}>
        <div className={styles.darkWrapper}>
          <h1 className={styles.title}>Find a space for your stuff</h1>
        </div>
      </div>
    );
  }
}
