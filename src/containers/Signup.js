import { connect } from 'react-redux';
import { signup } from '../actions/Signup';
import Signup from '../components/Signup';

const mapStateToProps = () => ({ test: 'test' });

const mapDispatchToProps = dispatch => ({
  onSubmit: formData => dispatch(signup(formData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);
