/* eslint react/prefer-stateless-function: 0 */
import React from 'react';
import fetch from 'isomorphic-fetch';
import cookie from 'react-cookie';
import cookies from '../utilities/cookies';
import BanButton from '../components/BanButton';


export default class OtherInformation extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      user: '',
      photoURL: '',
      address: '',
      creditcard: '',
      uid: this.props.params.uid,
      admin: JSON.parse(cookie.load(cookies.admin)),
      phone: '',
    };
    this.componentDidMount = this.componentDidMount.bind(this);
    this.requestPhone = this.requestPhone.bind(this);
    this.blockUser = this.blockUser.bind(this);
  }


  componentDidMount() {
    const uid = this.props.params.uid;
    fetch(`/api/user/${uid}`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
      },
    }).then(response => response.json().then((resp) => {
      const json = resp.user;
      if (json.email) {
        this.setState({
          email: json.username,
        });
      }
      if (json.avatar) {
        this.setState({
          photoURL: json.avatar,
        });
      }
      if (json.username) {
        this.setState({
          user: json.username,
        });
      }
      if (json.address) {
        this.setState({
          address: json.address,
        });
      }
      if (json.phone) {
        this.setState({
          phone: json.phone,
        });
      }
    }));
  }


  requestPhone(e) {
    e.preventDefault();
    const uid = this.props.params.uid;
    fetch(`/api/user/${uid}/phone`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
      },
    }).then(() => alert('🤠'))
    .catch(() => alert('💩'));
  }

  blockUser(e) {
    e.preventDefault();
    const uid = this.props.params.uid;
    fetch(`/api/user/${uid}/block`, {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
      },
    }).then(() => alert('🤠'))
    .catch(() => alert('💩'));
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  render() {
    return (
      <form role="form" onSubmit={this.handleLogin}>
        <h3>Update Information</h3>
        <div className="form-group">
          <label htmlFor="email">Email</label>
          <input
            readOnly
            type="text"
            name="email"
            onChange={this.handleInputChange}
            value={this.state.email}
          />
        </div>
        <div className="form-group">
          <label htmlFor="user">Username</label>
          <input
            readOnly
            type="text"
            name="user"
            onChange={this.handleInputChange}
            value={this.state.user}
          />
        </div>
        <div className="form-group">
          <label htmlFor="address">Address</label>
          <input
            readOnly
            type="text"
            name="address"
            onChange={this.handleInputChange}
            value={this.state.address}
          />
        </div>
        <div className="form-group">
          <label htmlFor="phone">Phone Number</label>
          <input
            readOnly
            type="text"
            name="phone"
            onChange={this.handleInputChange}
            value={this.state.phone}
          />
        </div>
        <div className="form-group">
          <label htmlFor="photoURL">Avatar</label>
          <input
            readOnly
            type="text"
            name="photoURL"
            onChange={this.handleInputChange}
            value={this.state.photoURL}
          />
          <img
            alt=""
            src={this.state.photoURL}
          />
        </div>
        <div>
          {this.state.admin ?
            <BanButton uid={this.state.uid} admin={this.state.admin} />
           : null}
        </div>
        {this.state.phone ?
        ''
        : <button onClick={this.requestPhone}>Request Phone</button>}
        <button onClick={this.blockUser}>Block</button>
      </form>
    );
  }
}

OtherInformation.propTypes = {
  params: React.PropTypes.shape({
    uid: React.PropTypes.string.isRequired,
  }).isRequired,
};
