import { connect } from 'react-redux';
import Posting from '../components/Posting';
import { createPosting, getPosting, setPosting, createContract } from '../actions/Posting';

const mapStateToProps = state => ({
  postings: state.postings.publicPostings,
  posting: state.posting.posting,
  token: state.user.token,
});

const mapDispatchToProps = dispatch => ({
  onCreatePosting: (token, formData) => dispatch(createPosting(token, formData)),
  onGetPosting: (token, postingId) => dispatch(getPosting(token, postingId)),
  onSetPosting: posting => dispatch(setPosting(posting)),
  onSubmit: (token, formData) => dispatch(createContract(token, formData)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Posting);
