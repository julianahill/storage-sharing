import { connect } from 'react-redux';
import Postings from '../components/Postings';
import { load } from '../actions/Postings';

const mapStateToProps = state => ({
  postings: state.postings.publicPostings,
  myPostings: state.postings.myPostings,
  token: state.user.token,
});

const mapDispatchToProps = dispatch => ({
  onLoad: token => dispatch(load(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Postings);
