import { connect } from 'react-redux';
import Contracts from '../components/Contracts';
import { load } from '../actions/Contracts';

const mapStateToProps = state => ({
  ownerContracts: state.contracts.ownerContracts,
  renterContracts: state.contracts.renterContracts,
  token: state.user.token,
});

const mapDispatchToProps = dispatch => ({
  onLoad: token => dispatch(load(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Contracts);
