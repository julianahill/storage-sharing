import { connect } from 'react-redux';
import { saveCreditCard, savePersonalDetail } from '../actions/Settings';
import { loadCard } from '../actions/Card';
import Settings from '../components/Settings';

const mapStateToProps = state => ({
  token: state.user.token,
  firstName: state.user.self.firstName,
  lastName: state.user.self.lastName,
  address: state.user.self.address,
  avatar: state.user.self.avatar,
  expMonth: state.card.expMonth,
  expYear: state.card.expYear,
  cardNumber: state.card.cardNumber,
});

const mapDispatchToProps = dispatch => ({
  saveCreditCard: (card, token) => dispatch(saveCreditCard(card, token)),
  savePersonalDetail: (details, token) => dispatch(savePersonalDetail(details, token)),
  loadCard: token => dispatch(loadCard(token)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Settings);
