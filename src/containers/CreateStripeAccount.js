import { connect } from 'react-redux';
import CreateStripeAccount from '../components/CreateStripeAccount';
import { saveStripeAccount } from '../actions/User';

const mapDispatchToProps = dispatch => ({
  saveStripeAccount: (id, token) => dispatch(saveStripeAccount(id, token)),
});

const mapStateToProps = state => ({
  token: state.user.token,
});

export default connect(mapStateToProps, mapDispatchToProps)(CreateStripeAccount);
