/* eslint react/prefer-stateless-function: 0 */
import React from 'react';
import fetch from 'isomorphic-fetch';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router';
import cookies from '../utilities/cookies';

export default class Information extends React.Component {
  static passwordReset(event) {
    event.preventDefault();
    fetch('/api/password_reset', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
      },
    }).then(response => response.json().then(() => {
    }));
  }

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      user: '',
      photoURL: '',
      address: '',
      phone: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.updateUser = this.updateUser.bind(this);
  }

  componentDidMount() {
    fetch('/api/user', {
      method: 'GET',
      headers: {
        Accept: 'application/json',
        Authorization: cookie.load(cookies.token),
      },
    }).then(response => response.json().then((json) => {
      if (json.email) {
        this.setState({
          email: json.email,
        });
      }
      if (json.photoURL) {
        this.setState({
          photoURL: json.photoURL,
        });
      }
      if (json.user) {
        this.setState({
          user: json.user,
        });
      }
      if (json.address) {
        this.setState({
          address: json.address,
        });
      }
      if (json.phone) {
        this.setState({
          phone: json.phone,
        });
      }
    }));
  }

  handleInputChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;

    this.setState({
      [name]: value,
    });
  }

  updateUser(event) {
    event.preventDefault();
    const formData = {
      email: this.state.email,
      user: this.state.user,
      password: this.state.pass,
      address: this.state.address,
      creditcard: this.state.creditcard,
      photoURL: this.state.photoURL,
      phone: this.state.phone,
    };
    return fetch('/api/user/update', {
      method: 'PUT',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        Authorization: cookie.load(cookies.token),
      },
      body: JSON.stringify(formData),
    }).then(response => response.json().then(() => {
    }));
  }

  /* eslint-disable */
  deleteUser(event) {
    event.preventDefault();
    return fetch('/api/user/delete', {
      method: 'DELETE',
      headers: {
        Authorization: cookie.load(cookies.token),
      },
    }).then(() => {
      browserHistory.push('/');
    });
  }
  /* eslint-disable */

  render() {
    return (
      <div className="container">
        <form role="form" onSubmit={this.handleLogin}>
          <h3>Update Information</h3>
          <div className="form-group">
            <label htmlFor="email">Email</label>
            <input
              type="text"
              name="email"
              onChange={this.handleInputChange}
              value={this.state.email}
            />
          </div>
          <div className="form-group">
            <label htmlFor="password">Password</label>
            <input
              type="text"
              name="pass"
              onChange={this.handleInputChange}
              value={this.state.pass}
            />
          </div>
          <div className="form-group">
            <label htmlFor="user">Username</label>
            <input
              type="text"
              name="user"
              onChange={this.handleInputChange}
              value={this.state.user}
            />
          </div>
          <div className="form-group">
            <label htmlFor="address">Address</label>
            <input
              type="text"
              name="address"
              onChange={this.handleInputChange}
              value={this.state.address}
            />
          </div>
          <div className="form-group">
            <label htmlFor="phone">Phone</label>
            <input
              type="text"
              name="phone"
              onChange={this.handleInputChange}
              value={this.state.phone}
            />
          </div>
          <div className="form-group">
            <label htmlFor="photoURL">Avatar</label>
            <input
              type="text"
              name="photoURL"
              onChange={this.handleInputChange}
              value={this.state.photoURL}
            />
            <img
              alt=""
              src={this.state.photoURL}
            />
          </div>
          <div>
            <button onClick={this.passwordReset}>Reset Password</button>
          </div>
          <button onClick={this.updateUser}>Update</button>
          <div>
            <button onClick={this.deleteUser}>Delete</button>
          </div>
        </form>
      </div>
    );
  }
}
