/* eslint react/prefer-stateless-function: 0, react/no-array-index-key: 0 */
import React from 'react';
import GoogleMap from 'google-map-react';
import { Link } from 'react-router';
import styles from '../styles/Postings.css';

const MarkerComponent = ({ id }) =>
  <div className={styles.marker}>
    <Link className={styles.markerNum} to={`/postings/${id}`}>1</Link>
  </div>;

const FrequencyString = {
  day: 'Daily',
  week: 'Weekly',
  month: 'Monthly',
};

const ShortFrequency = {
  day: 'day',
  week: 'wk',
  month: 'mo',
};

export default class Postings extends React.Component {

  constructor() {
    super();

    this.state = {
      myPostings: [],
      postings: [],
      latitude: 40.426726,
      longitude: -86.909453,
    };
  }

  componentWillMount() {
    this.props.onLoad(this.props.token);
  }

  handleLatChange(e) {
    this.setState({ latitude: e.target.value });
  }

  handleLngChange(e) {
    this.setState({ longitude: e.target.value });
  }

  renderMap() {
    const mapStyle = {
      height: '100%',
      width: '100%',
      position: 'relative',
    };
    const Markers = this.props.postings.map(posting => (
      <MarkerComponent
        lat={posting.latitude}
        lng={posting.longitude}
        id={posting.id}
      />
    ));
    return (
      <div className={styles.map}>
        <GoogleMap
          style={mapStyle}
          defaultCenter={{ lat: this.state.latitude, lng: this.state.longitude }}
          defaultZoom={15}
        >
          {Markers}
        </GoogleMap>
      </div>
    );
  }

  renderMyPostings() {
    if (this.props.myPostings.length > 0) {
      const listMyPostings = this.props.myPostings.map((posting, index) => (
        <li key={index}>
          Title: {posting.title} <br />
          Owner: {posting.ownerName}<br />
          Description: {posting.description} <br />
          Image: <img src={posting.images} alt="" /> <br />
          {/* Latitude: {posting.latitude} <br />
          Longitude: {posting.longitude} <br /> */}
          Address: <br />{`${posting.street}
          ${posting.city}, ${posting.state}
          ${posting.zip}`} <br />
          Capacity: {posting.capacity} <br />
          Rate: {posting.rate} <br />
          Frequency: {FrequencyString[posting.frequency]} <br />
        </li>
      ));
      return (
        <div>
          <h3>My Postings</h3>
          <ul>{listMyPostings}</ul>
        </div>
      );
    }
    return (
      <div>
        <h3>My Postings</h3>
        <span>You have 0 postings</span>
      </div>
    );
  }

  renderPostings() {
    if (this.props.postings.length > 0) {
      const listPostings = this.props.postings.map((posting, index) => (
        <li className={styles.card} key={index}>
          <Link to={`/postings/${posting.id}`}>
            <img className={styles.postImage} src={posting.images} alt="" />
            <span className={styles.postImageGradient} />
            <div className={styles.cardText}>
              <h1 className={styles.postPrice}>{`$${posting.rate}/${ShortFrequency[posting.frequency]}`}</h1>
              <br />
              <h2 className={styles.postTitle}>{posting.title}</h2>
              <br />
              <p className={styles.postSubtitle}>{posting.capacity}</p>
            </div>
          </Link>
        </li>
      ));
      return (
        <div>
          <ul className={styles.cardGrid}>
            {listPostings}
            <span className={styles.card} />
            <span className={styles.card} />
          </ul>
        </div>
      );
    }
    return (
      <div>
        <h3>Postings</h3>
        <span>There are 0 postings</span>
      </div>
    );
  }

  render() {
    return (
      <div className={styles.container}>
        {/* {this.renderMyPostings()} */}
        <div className={styles.postingsContainer}>
          {this.renderPostings()}
        </div>
        {this.renderMap()}
      </div>
    );
  }
}

Postings.propTypes = {
  onLoad: React.PropTypes.func.isRequired,
  postings: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      title: React.PropTypes.string.isRequired,
      ownerName: React.PropTypes.string.isRequired,
      description: React.PropTypes.string.isRequired,
      image: React.PropTypes.string,
      latitude: React.PropTypes.number.isRequired,
      longitude: React.PropTypes.number.isRequired,
      capacity: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
    }),
  ),
  myPostings: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      title: React.PropTypes.string.isRequired,
      ownerName: React.PropTypes.string.isRequired,
      description: React.PropTypes.string.isRequired,
      image: React.PropTypes.string,
      latitude: React.PropTypes.number.isRequired,
      longitude: React.PropTypes.number.isRequired,
      capacity: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
    }),
  ),
  token: React.PropTypes.string.isRequired,
};
MarkerComponent.propTypes = {
  id: React.PropTypes.string.isRequired,
};

Postings.defaultProps = {
  postings: [],
  myPostings: [],
};
