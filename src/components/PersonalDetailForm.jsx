/* eslint global-require: 0 */
import React from 'react';
import styles from '../styles/CreditCardForm.css';

export default class PersonalDetailForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      firstName: props.firstName || '',
      lastName: props.lastName || '',
      address: props.address || '',
      avatar: props.avatar || '',
      phone: props.phone || '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleChangeFileUrl = this.handleChangeFileUrl.bind(this);
  }

  componentDidMount() {
    const filepicker = require('filepicker-js');
    filepicker.constructWidget(this.filepicker);
    this.filepicker.addEventListener('change', this.handleChangeFileUrl, false);
  }

  componentWillUnmount() {
    this.filepicker.removeEventListener('change', this.handleChangeFileUrl, false);
  }

  handleChangeFileUrl(e) {
    this.setState({ avatar: e.target.value });
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const details = {
      firstName: this.state.firstName ? this.state.firstName : null,
      lastName: this.state.lastName ? this.state.lastName : null,
      address: this.state.address ? this.state.address : null,
      avatar: this.state.avatar ? this.state.avatar : null,
      phone: this.state.phone ? this.state.phone : null,
    };
    this.props.savePersonalDetail(details, this.props.token);
  }

  render() {
    return (
      <form className={styles.form} role="form">
        <label htmlFor="firstName">First Name</label>
        <input
          type="text"
          name="firstName"
          onChange={this.handleInputChange}
          value={this.state.firstName}
        />
        <label htmlFor="lastName">Last Name</label>
        <input
          type="text"
          name="lastName"
          onChange={this.handleInputChange}
          value={this.state.lastName}
        />
        <label htmlFor="address">Address</label>
        <textarea
          name="address"
          onChange={this.handleInputChange}
          value={this.state.address}
        />
        <label htmlFor="avatar">Profile Picture</label>
        <input
          data-fp-button-text={this.state.imageUploaded ? 'Change Image' : 'Pick Image'}
          name="photoUrl"
          data-fp-apikey="Avgki0eZhQjl8MMjTVSOVz"
          ref={(c) => { this.filepicker = c; }}
          type="filepicker"
        />
        <br />
        <label htmlFor="phone">Phone Number</label>
        <input
          name="phone"
          onChange={this.handleInputChange}
          value={this.state.phone}
        />
        <br />
        <button className={styles.save_button} onClick={this.handleSubmit}>Save</button>
      </form>
    );
  }
}

PersonalDetailForm.propTypes = {
  avatar: React.PropTypes.string,
  firstName: React.PropTypes.string,
  lastName: React.PropTypes.string,
  address: React.PropTypes.string,
  token: React.PropTypes.string.isRequired,
  phone: React.PropTypes.string.isRequired,
  savePersonalDetail: React.PropTypes.func.isRequired,
};

PersonalDetailForm.defaultProps = {
  firstName: '',
  lastName: '',
  address: '',
  avatar: '',
  phone: '',
};