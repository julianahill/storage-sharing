/* eslint react/prefer-stateless-function: 0 */
/* eslint global-require: 0 */
import React from 'react';
// import fetch from 'isomorphic-fetch';
// import cookie from 'react-cookie';
// import { browserHistory } from 'react-router';
// import cookies from '../utilities/cookies';
import styles from '../styles/CreditCardForm.css';

export default class CreatePosting extends React.Component {
  // https://facebook.github.io/react/docs/forms.html

  constructor(props) {
    super(props);

    this.state = {
      title: '',
      description: '',
      images: '',
      // latitude: '0',
      // longitude: '0',
      capacity: '',
      street: '',
      city: '',
      state: '',
      zip: '',
      rate: '',
      frequency: 'day',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCreatePosting = this.handleCreatePosting.bind(this);
    this.handleChangeFileUrl = this.handleChangeFileUrl.bind(this);
    this.handleFrequencyChange = this.handleFrequencyChange.bind(this);
  }

  componentDidMount() {
    const filepicker = require('filepicker-js');
    filepicker.constructWidget(this.filepicker);
    this.filepicker.addEventListener('change', this.handleChangeFileUrl, false);
  }

  componentWillUnmount() {
    this.filepicker.removeEventListener('change', this.handleChangeFileUrl, false);
  }

  handleChangeFileUrl(e) {
    this.setState({ images: e.target.value });
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleFrequencyChange(event) {
    this.setState({ frequency: event.target.value });
  }

  handleCreatePosting(event) {
    event.preventDefault();
    const formData = {
      title: this.state.title,
      description: this.state.description,
      images: this.state.images,
      // latitude: this.state.latitude,
      // longitude: this.state.longitude,
      street: this.state.street,
      city: this.state.city,
      state: this.state.state,
      zip: this.state.zip,
      capacity: this.state.capacity,
      rate: this.state.rate,
      frequency: this.state.frequency,
    };

    this.props.onCreatePosting(this.props.token, formData);
  }

  render() {
    return (
      <form className={styles.form} role="form" onSubmit={this.handleCreatePosting}>
        <h1 className={styles.title}>Create Posting</h1>
        <h3>{this.state.status}</h3>
        <div className="form-group">
          <label htmlFor="title">Title</label>
          <input
            type="text"
            placeholder="Great location! Near campus"
            name="title"
            onChange={this.handleInputChange}
            value={this.state.title}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="description">Description</label>
          <input
            type="text"
            placeholder="Near campus!"
            name="description"
            onChange={this.handleInputChange}
            value={this.state.description}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="images">Images</label>
          <input
            data-fp-button-text={this.state.images ? 'Change Image' : 'Pick Image'}
            name="images"
            data-fp-apikey="Avgki0eZhQjl8MMjTVSOVz"
            ref={(c) => { this.filepicker = c; }}
            type="filepicker"
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="street">Street</label>
          <input
            type="text"
            name="street"
            onChange={this.handleInputChange}
            value={this.state.street}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="city">City</label>
          <input
            type="text"
            name="city"
            onChange={this.handleInputChange}
            value={this.state.city}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="state">State</label>
          <input
            type="text"
            name="state"
            onChange={this.handleInputChange}
            value={this.state.state}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="zip">Zip</label>
          <input
            type="number"
            name="zip"
            onChange={this.handleInputChange}
            value={this.state.zip}
            required
          />
        </div>
        {/* <div className="form-group">
          <label htmlFor="latitude">Latitude</label>
          <input
            type="text"
            placeholder="40.427728"
            name="latitude"
            onChange={this.handleInputChange}
            value={this.state.latitude}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="longitude">Longitude</label>
          <input
            type="text"
            placeholder="-86.916975"
            name="longitude"
            onChange={this.handleInputChange}
            value={this.state.longitude}
            required
          />
        </div> */}
        <div className="form-group">
          <label htmlFor="capacity">Capacity</label>
          <input
            type="text"
            placeholder="50 sq ft"
            name="capacity"
            onChange={this.handleInputChange}
            value={this.state.capacity}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="rate">Rate</label>
          <input
            type="number"
            min="0.01"
            step="0.01"
            placeholder="$50"
            name="rate"
            onChange={this.handleInputChange}
            value={this.state.rate}
            required
          />
        </div>
        <div className="form-group">
          <label htmlFor="frequency">Frequency</label>
          <select name="frequency" defaultValue="day" onChange={this.handleFrequencyChange}>
            <option value="day">Daily</option>
            <option value="week">Weekly</option>
            <option value="month">Monthly</option>
          </select>
          <button className={styles.save_button} type="submit">Create Posting</button>
        </div>
      </form>
    );
  }
}

CreatePosting.propTypes = {
  onCreatePosting: React.PropTypes.func.isRequired,
  token: React.PropTypes.string.isRequired,
};
