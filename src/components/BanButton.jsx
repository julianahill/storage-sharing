/* eslint react/prefer-stateless-function: 0 */
/* eslint no-alert: 0 */
import React from 'react';
import fetch from 'isomorphic-fetch';
import cookie from 'react-cookie';
import cookies from '../utilities/cookies';


export default class BanButton extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      uid: this.props.uid,
      admin: this.props.uid,
    };
    this.ban = this.ban.bind(this);
  }


  ban(e) {
    e.preventDefault();
    if (confirm(`Ban ${this.state.uid}?`)) {
      fetch(`/api/user/${this.state.uid}/ban`, {
        method: 'GET',
        headers: {
          Accept: 'application/json',
          Authorization: cookie.load(cookies.token),
        },
      }).then(() => alert('😵BANNED😵'));
    }
  }

  render() {
    return (
      <button onClick={this.ban}>Ban</button>
    );
  }
}

BanButton.propTypes = {
  uid: React.PropTypes.string.isRequired,
  admin: React.PropTypes.bool.isRequired,
};
