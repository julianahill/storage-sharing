import React from 'react';
import { Link } from 'react-router';

import styles from '../styles/Navbar.css';

const LandingRightNav = (isLoggedIn, logOut) => {
  if (isLoggedIn) {
    return (
      <ul className={styles.rightNav}>
        <li><Link className={styles.landingNavLink} to="/postings/create">List a space</Link></li>
        <li><Link className={styles.landingNavLink} to="/contracts">Contracts</Link></li>
        <li><Link className={styles.landingNavLink} to="/messages">Messages</Link></li>
        {/* <li><Link className={styles.landingNavLink} to="/sendmessage">Send Message</Link></li>
        <li><Link className={styles.landingNavLink} to="/create">Create Posting</Link></li> */}
        <li><Link className={styles.landingNavLink} to="/postings">Postings</Link></li>
        <li><Link className={styles.landingNavLink} to="/profile">Profile</Link></li>
        {/* <li><Link to="/complaint">Register a complaint</Link></li> */}
        <li><Link className={styles.landingNavLink} to="/bugs">Report a bug</Link></li>
        <li><Link className={styles.landingNavLink} to="/settings">Settings</Link></li>
        <li><Link className={styles.landingNavLink} to="/login" onClick={logOut}>Log Out</Link></li>
      </ul>
    );
  }
  return (
    <ul className={styles.rightNav}>
      {/* <li><Link className={styles.landingNavLink} to="/create">Create Posting</Link></li> */}
      <li><Link className={styles.landingNavLink} to="/signup">Sign Up</Link></li>
      <li><Link className={styles.landingNavLink} to="/login">Log In</Link></li>
    </ul>
  );
};

const LandingNavbar = props => (
  <nav className={styles.landingNavbar}>
    <div className={styles.container}>
      <Link className={styles.landingNavBrand} to="/">
        <img
          alt="logo"
          className={styles.logo}
          src="/images/logo-white.svg"
        />
        <span
          className={styles.brand}
        >storestuff.club</span>
      </Link>
      {LandingRightNav(props.isLoggedIn, props.logOut)}
    </div>
  </nav>
);

LandingNavbar.propTypes = {
  isLoggedIn: React.PropTypes.bool.isRequired,
  logOut: React.PropTypes.func.isRequired,
};

export default LandingNavbar;
