/* eslint jsx-a11y/no-static-element-interactions: 0 */

import React from 'react';
import { Link } from 'react-router';
import CreditCardForm from './CreditCardForm';
import PersonalDetailForm from './PersonalDetailForm';
import form_style from '../styles/CreditCardForm.css';

const ExpandItem = (props) => {
  if (props.active) {
    return (
      <div>
        <a
          onClick={props.toggle}
          className="list-group-item active"
        >
        <div className={form_style.expandable}>{props.title}</div>
        </a>
        {props.children}
      </div>
    );
  }
  return (
    <a onClick={props.toggle} className="list-group-item">
      <div className={form_style.expandable}>{props.title}</div>
    </a>
  );
};

ExpandItem.propTypes = {
  active: React.PropTypes.bool.isRequired,
  children: React.PropTypes.node.isRequired,
  toggle: React.PropTypes.func.isRequired,
  title: React.PropTypes.string.isRequired,
  phone: React.PropTypes.string.isRequired,
};

export default class Settings extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      active: null,
    };
    this.toggleItem = this.toggleItem.bind(this);
  }

  componentWillMount() {
    this.props.loadCard(this.props.token);
  }


  toggleItem(type) {
    this.setState({ active: type === this.state.active ? null : type });
  }

  render() {
    const types = {
      CARD: 'CARD',
      PASS: 'PASS',
      DETAILS: 'DETAILS',
    };
    return (
      <div className="container">
        <h1 className={form_style.title}>Settings</h1>
        <div className="list-group">
          <ExpandItem
            active={this.state.active === types.DETAILS}
            toggle={() => this.toggleItem(types.DETAILS)}
            title="Update Personal Details"
          >
            <PersonalDetailForm
              token={this.props.token}
              firstName={this.props.firstName}
              lastName={this.props.lastName}
              address={this.props.address}
              avatar={this.props.avatar}
              phone={this.props.phone}
              savePersonalDetail={this.props.savePersonalDetail}
            />
          </ExpandItem>
          <br/>
          <ExpandItem
            active={this.state.active === types.CARD}
            toggle={() => this.toggleItem(types.CARD)}
            title="Update Credit Card"
          >
            <CreditCardForm
              expMonth={this.props.expMonth}
              expYear={this.props.expYear}
              cardNumber={this.props.cardNumber}
              loadCard={this.props.loadCard}
              token={this.props.token} saveCreditCard={this.props.saveCreditCard}
            />
          </ExpandItem>
          <br/>
          <div className={form_style.expandable}>
            <Link to="/resetpassword" className={form_style.link}>Reset Password</Link>
          </div>
          <br/>
          <div className={form_style.expandable}>
            <Link to="/complaint" className={form_style.link}>File a Complaint</Link>
          </div>
          <br/>
          <div className={form_style.expandable}>
            <Link to="/bugs" className={form_style.link}>Report a Bug</Link>
          </div>
          <br/>
        </div>
      </div>
    );
  }
}

Settings.propTypes = {
  expMonth: React.PropTypes.string,
  expYear: React.PropTypes.string,
  cardNumber: React.PropTypes.string,
  loadCard: React.PropTypes.func.isRequired,
  firstName: React.PropTypes.string,
  lastName: React.PropTypes.string,
  address: React.PropTypes.string,
  avatar: React.PropTypes.string,
  phone: React.PropTypes.string,
  token: React.PropTypes.string.isRequired,
  saveCreditCard: React.PropTypes.func.isRequired,
  savePersonalDetail: React.PropTypes.func.isRequired,
};

Settings.defaultProps = {
  firstName: '',
  lastName: '',
  address: '',
  avatar: '',
  expMonth: '',
  expYear: '',
  cardNumber: '',
};
