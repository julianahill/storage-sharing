/* eslint react/self-closing-comp: 0, react/no-array-index-key: 0 */
import React from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import styles from '../styles/Profile.css';

const FrequencyString = {
  day: 'Daily',
  week: 'Weekly',
  month: 'Monthly',
};

export default class Profile extends React.Component {

  componentWillMount() {
    this.props.onLoad(this.props.token);
  }

  render() {
    const myPostings = this.props.myPostings ? this.props.myPostings.map((posting, index) => (
      <li key={index}>
        {/* Title: {posting.title} <br />
        Owner: {posting.ownerName}<br />
        Description: {posting.description} <br />
        <img src={posting.images} alt="" /> <br /> */}
        {/* Latitude: {posting.latitude} <br />
        Longitude: {posting.longitude} <br /> */}
        {/* Address: <br />{`${posting.street}
        ${posting.city}, ${posting.state}
        ${posting.zip}`} <br />
        Capacity: {posting.capacity} <br />
        Rate: {posting.rate} <br />
        Frequency: {FrequencyString[posting.frequency]} <br /> */}
        <h1 className={styles.title}>{posting.title}</h1>
        <div className={styles.postingscontainer}>
          <div className={classnames(styles.column, styles.leftColumn)}>
            <img className={styles.postImage} src={posting.images} alt="" />
          </div>
          <div className={classnames(styles.column, styles.rightColumn)}>
            <label>Owner: {posting.ownerName}</label>
            <label>Description: {posting.description}</label>
            {/* <p>Latitude: {posting.latitude}</p>
            <p>Longitude: {posting.longitude}</p> */}
            <label>Address: <br />
              {`${posting.street}\n${posting.city}, ${posting.state}\n${posting.zip}`}
            </label>
            <label>Capacity: {posting.capacity}</label>
            <label>Rate: {posting.rate}</label>
            <label>Frequency: {FrequencyString[posting.frequency]}</label>
          </div>
        </div>
      </li>
    )) : '';
    return (
      <div className={styles.container}>
        <div className={styles.center}>
          <img className={styles.card} alt=""></img>
          <img className={styles.avatar} src={this.props.avatar || ''} alt="" />
          <div className={styles.info}>
            <h1 className={styles.name}>
              {`${this.props.firstName || ''} ${this.props.lastName || ''}`}
            </h1>
            <h3 className={styles.other}>{`Email: ${this.props.username || ''}`}</h3>
            <h3 className={styles.other}>{`Address: ${this.props.address || ''}`}</h3>
            <h3 className={styles.other}>{`Phone Number: ${this.props.phone || ''}`}</h3>
            <Link className={classnames(styles.button, styles.link)} to="/sendmessage">
              Send a Message
            </Link>
            <br />
            <br />
            <br />
            <br />
          </div>
          <ul>{myPostings}</ul>
        </div>
      </div>
    );
  }
}

Profile.propTypes = {
  myPostings: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      title: React.PropTypes.string.isRequired,
      ownerName: React.PropTypes.string.isRequired,
      description: React.PropTypes.string.isRequired,
      image: React.PropTypes.string,
      latitude: React.PropTypes.number.isRequired,
      longitude: React.PropTypes.number.isRequired,
      capacity: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
    }),
  ),
  token: React.PropTypes.string.isRequired,
  onLoad: React.PropTypes.func.isRequired,
  firstName: React.PropTypes.string,
  lastName: React.PropTypes.string,
  address: React.PropTypes.string,
  avatar: React.PropTypes.string,
  username: React.PropTypes.string,
  phone: React.PropTypes.string,
};

Profile.defaultProps = {
  firstName: '',
  lastName: '',
  address: '',
  avatar: '',
  username: '',
  phone: '',
  myPostings: [],
};
