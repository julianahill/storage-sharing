/* eslint react/prefer-stateless-function: 0, react/no-array-index-key: 0 */
import React from 'react';
import { Link } from 'react-router';
import classnames from 'classnames';
import styles from '../styles/Posting.css'

export default class Contracts extends React.Component {

  constructor() {
    super();

    this.state = {
      ownerContracts: [],
      renterContracts: [],
    };
  }

  componentWillMount() {
    this.props.onLoad(this.props.token);
  }

  renderOwnerContracts() {
    if (this.props.ownerContracts.length > 0) {
      const listOwnerContracts = this.props.ownerContracts.map((contract, index) => (
      <li key={index}>
        <label>Owner: {contract.ownerName}</label>
        <label>Renter: {contract.renterName}</label>
        <span>{contract.postingTitle}</span>
        <label>StartDate: {contract.startDate }</label>
        <label>EndDate: { contract.endDate }</label>
        <label>Status: { contract.accepted ? 'Accepted' : 'Pending'}</label>
      </li>
      ));
      return (
        <div>
          <h3>Owner Contracts</h3>
          <br/>
          <ul>{listOwnerContracts}</ul>
        </div>
      );
    }
    return (
      <div>
        <label>Owner Contracts</label>
        <br/>
        <span>You have 0 contracts</span>
      </div>
    );
  }

  renderRenterContracts() {
    if (this.props.renterContracts.length > 0) {
      const listRenterContracts = this.props.renterContracts.map((contract, index) => (
        <li key={index}>
          <label>Owner: {contract.ownerName}</label>
          <label>Renter: {contract.renterName}</label>
          <Link to={`/postings/${contract.postingId}`} className={styles.linkList}>
            {contract.postingTitle}
          </Link>
          <label>Rate: {contract.rate} / {contract.frequency}</label>
          <label>StartDate: {contract.startDate }</label>
          <label>EndDate: { contract.endDate }</label>
          <label>Status: { contract.accepted ? 'Accepted' : 'Pending'}</label>
        </li>
      ));
      return (
        <div>
          <label>Renter Contracts</label>
          <br/>
          <ul>{listRenterContracts}</ul>
        </div>
      );
    }
    return (
      <div>
        <h3>Renter Contracts</h3>
        <br/>
        <span>There are 0 contracts</span>
      </div>
    );
  }

  render() {
    return (
    <div className={styles.form}>
      <h1 className={styles.title}>Your Contracts</h1>
      <div className={classnames('container', styles.container)}>
        <div className={classnames(styles.column, styles.owned)}>
            {this.renderOwnerContracts()}
          </div>
          <div className={classnames(styles.column, styles.renting)}>
            {this.renderRenterContracts()}
          </div>
        </div>
      </div>
    );
  }
}

Contracts.propTypes = {
  onLoad: React.PropTypes.func.isRequired,
  renterContracts: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      ownerId: React.PropTypes.string,
      renterId: React.PropTypes.string,
      ownerName: React.PropTypes.string.isRequired,
      renterName: React.PropTypes.string.isRequired,
      postingTitle: React.PropTypes.string.isRequired,
      postingId: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
      startDate: React.PropTypes.string.isRequired,
      endDate: React.PropTypes.string.isRequired,
    }),
  ),
  ownerContracts: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      ownerId: React.PropTypes.string,
      renterId: React.PropTypes.string,
      ownerName: React.PropTypes.string.isRequired,
      renterName: React.PropTypes.string.isRequired,
      postingTitle: React.PropTypes.string.isRequired,
      postingId: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
      startDate: React.PropTypes.string.isRequired,
      endDate: React.PropTypes.string.isRequired,
    }),
  ),
  token: React.PropTypes.string.isRequired,
};

Contracts.defaultProps = {
  renterContracts: [],
  ownerContracts: [],
};
