import React from 'react';
import styles from '../styles/Form.css';
import { Link } from 'react-router';

export default class ResetPassword extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleReset = this.handleReset.bind(this);
  }

  handleInputChange(event) {
    this.setState({ email: event.target.value });
  }

  handleReset(event) {
    event.preventDefault();
    const formData = {
      email: this.state.email,
    };
    this.props.onSubmit(formData);
  }

  render() {
    return (
      <div className={styles.container}>
        <h1 className={styles.title}>Reset Your Password</h1>
        <form className={styles.card} role="form" onSubmit={this.handleReset}>
          <div className="form-group">
            <label className={styles.label} htmlFor="user">Email</label>
            <input
              className={styles.input}
              type="text"
              placeholder=""
              name="user"
              onChange={this.handleInputChange}
              value={this.state.email}
            />
          </div>
          <div className={styles.buttonContainer}>
            <Link className={styles.link} to="/login">Back to Login</Link>
            <button className={styles.button} type="submit">Get Temporary Password</button>
          </div>
        </form>
      </div>
    );
  }
}

ResetPassword.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
};
