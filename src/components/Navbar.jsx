import React from 'react';
import { Link } from 'react-router';

import styles from '../styles/Navbar.css';

const RightNav = (isLoggedIn, logOut) => {
  if (isLoggedIn) {
    return (
      <ul className={styles.rightNav}>
        <li><Link className={styles.navLink} to="/postings/create">List a space</Link></li>
        <li><Link className={styles.navLink} to="/contracts">Contracts</Link></li>
        <li><Link className={styles.navLink} to="/messages">Messages</Link></li>
        {/* <li><Link className={styles.navLink} to="/sendmessage">Send Message</Link></li>
        <li><Link className={styles.navLink} to="/postings/create">Create Posting</Link></li> */}
        <li><Link className={styles.navLink} to="/postings">Postings</Link></li>
        <li><Link className={styles.navLink} to="/profile">Profile</Link></li>
        {/* <li><Link to="/complaint">Register a complaint</Link></li> */}
        {/* <li><Link className={styles.navLink} to="/bugs">Report a bug</Link></li> */}
        <li><Link className={styles.navLink} to="/settings">Settings</Link></li>
        <li><Link className={styles.navLink} to="/login" onClick={logOut}>Log Out</Link></li>
      </ul>
    );
  }
  return (
    <ul className={styles.rightNav}>
      <li><Link className={styles.navLink} to="/signup">Sign Up</Link></li>
      <li><Link className={styles.navLink} to="/login">Log In</Link></li>
    </ul>
  );
};

const Navbar = props => (
  <nav className={styles.navbar}>
    <div className={styles.container}>
      <Link className={styles.navBrand} to="/">
        <img
          alt="logo"
          className={styles.logo}
          src="/images/logo.svg"
        />
        <span
          className={styles.brand}
        >storestuff.club</span>
      </Link>
      {RightNav(props.isLoggedIn, props.logOut)}
    </div>
  </nav>
);

Navbar.propTypes = {
  isLoggedIn: React.PropTypes.bool.isRequired,
  logOut: React.PropTypes.func.isRequired,
};

export default Navbar;
