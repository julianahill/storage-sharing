import React from 'react';
import classnames from 'classnames';
import styles from '../styles/Form.css';

export default class Signup extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      firstName: '',
      lastName: '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSignup = this.handleSignup.bind(this);
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleSignup(event) {
    event.preventDefault();
    this.props.onSubmit({
      username: this.state.email,
      password: this.state.password,
      firstName: this.state.firstName,
      lastName: this.state.lastName,
    });
  }

  render() {
    return (
      <div className={styles.container}>
        <h1 className={styles.title}>Create an Account</h1>
        <form className={styles.card} role="form" onSubmit={this.handleSignup}>
          <div className={classnames('form-group', styles.col2)}>
            <div className={styles.column}>
              <label className={styles.label} htmlFor="email">First Name</label>
              <input
                className={styles.input}
                type="text"
                name="firstName"
                onChange={this.handleInputChange}
                value={this.state.firstName}
              />
            </div>
            <div className={styles.column}>
              <label className={styles.label} htmlFor="email">Last Name</label>
              <input
                className={styles.input}
                type="text"
                name="lastName"
                onChange={this.handleInputChange}
                value={this.state.lastName}
              />
            </div>
          </div>
          <div className="form-group">
            <label className={styles.label} htmlFor="email">Email</label>
            <input
              className={styles.input}
              type="text"
              placeholder="you@storestuff.club"
              name="email"
              onChange={this.handleInputChange}
              value={this.state.email}
            />
          </div>
          <div className="form-group">
            <label className={styles.label} htmlFor="pass">Password</label>
            <input
              className={styles.input}
              type="password"
              name="password"
              onChange={this.handleInputChange}
              value={this.state.password}
            />
          </div>
          <div className={styles.buttonContainer}>
            <div />
            <button className={styles.button} type="submit">Create Account</button>
          </div>
        </form>
      </div>
    );
  }
}

Signup.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
};
