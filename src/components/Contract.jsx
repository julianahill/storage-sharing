import React from 'react';
import _ from 'lodash';
import { Link } from 'react-router';

export default class Contract extends React.Component {

  constructor(props) {
    super(props);

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleAccept = this.handleAccept.bind(this);
  }

  componentWillMount() {
    const contracts = this.props.ownerContracts.concat(this.props.renterContracts);
    const contract = _.find(contracts, currentContract => (
      currentContract.id === this.props.params.contractId
    ));
    if (!contract) {
      this.props.onGetContract(this.props.token, this.props.params.contractId);
    } else {
      this.props.onSetContract(contract);
    }
  }

  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleAccept(event) {
    event.preventDefault();
    this.props.onAcceptContract(this.props.token, this.props.params.contractId);
  }

  render() {
    return (
      <div>
        Owner Name: {this.props.contract.ownerName} <br />
        Renter Name: {this.props.contract.renterName} <br />
        Posting Title: <Link to={`/postings/${this.props.contract.postingId}`}>{ this.props.contract.postingTitle}</Link> <br />
        Rate: {this.props.contract.rate} <br />
        Frequency: {this.props.contract.frequency} <br />
        StartDate: {this.props.contract.startDate } <br />
        EndDate: { this.props.contract.endDate } <br />
        Status: { this.props.contract.accepted ? 'Accepted' : 'Pending'} <br />
        <Button
          active={this.props.contract.ownerId === this.props.uid && !this.props.contract.accepted}
          handleAccept={this.handleAccept}
        />
      </div>
    );
  }
}

const Button = (props) => {
  if (props.active) {
    return <button onClick={props.handleAccept}>Accept</button>;
  }
  return null;
};

Button.propTypes = {
  active: React.PropTypes.bool.isRequired,
  handleAccept: React.PropTypes.func.isRequired,
};

Contract.propTypes = {
  onAcceptContract: React.PropTypes.func.isRequired,
  onSetContract: React.PropTypes.func.isRequired,
  onGetContract: React.PropTypes.func.isRequired,
  ownerContracts: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      ownerId: React.PropTypes.string,
      renterId: React.PropTypes.string,
      ownerName: React.PropTypes.string,
      renterName: React.PropTypes.string,
      postingTitle: React.PropTypes.string.isRequired,
      postingId: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
      startDate: React.PropTypes.string.isRequired,
      endDate: React.PropTypes.string.isRequired,
      accepted: React.PropTypes.bool.isRequired,
    }),
  ),
  renterContracts: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      ownerId: React.PropTypes.string,
      renterId: React.PropTypes.string,
      ownerName: React.PropTypes.string,
      renterName: React.PropTypes.string,
      postingTitle: React.PropTypes.string.isRequired,
      postingId: React.PropTypes.string.isRequired,
      rate: React.PropTypes.string.isRequired,
      frequency: React.PropTypes.string.isRequired,
      startDate: React.PropTypes.string.isRequired,
      endDate: React.PropTypes.string.isRequired,
      accepted: React.PropTypes.bool.isRequired,
    }),
  ),
  contract: React.PropTypes.shape({
    ownerId: React.PropTypes.string,
    renterId: React.PropTypes.string,
    ownerName: React.PropTypes.string,
    renterName: React.PropTypes.string,
    postingTitle: React.PropTypes.string,
    postingId: React.PropTypes.string,
    rate: React.PropTypes.string,
    frequency: React.PropTypes.string,
    startDate: React.PropTypes.string,
    endDate: React.PropTypes.string,
    accepted: React.PropTypes.bool,
  }).isRequired,
  params: React.PropTypes.shape({
    contractId: React.PropTypes.string.isRequired,
  }).isRequired,
  token: React.PropTypes.string.isRequired,
  uid: React.PropTypes.string,
};

Contract.defaultProps = {
  renterContracts: [],
  ownerContracts: [],
  uid: null,
};
