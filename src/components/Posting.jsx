import React from 'react';
import { Link } from 'react-router';
import _ from 'lodash';
import classnames from 'classnames';
import styles from '../styles/Posting.css';

function getFrequencyString(frequency) {
  switch (frequency) {
    case 'day':
      return 'Daily';
    case 'week':
      return 'Weekly';
    case 'month':
      return 'Monthly';
    default:
      return 'Error';
  }
}

export default class Posting extends React.Component {

  constructor() {
    super();

    this.state = {
      startDate: '',
      endDate: '',
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentWillMount() {
    const posting = _.find(this.props.postings, currentPosting => (
      currentPosting.id === this.props.params.postingId
    ));
    if (!posting) {
      this.props.onGetPosting(this.props.token, this.props.params.postingId);
    } else {
      this.props.onSetPosting(posting);
    }
  }


  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    this.props.onSubmit(this.props.token, {
      startDate: this.state.startDate,
      endDate: this.state.endDate,
      postingId: this.props.params.postingId,
      rate: this.props.posting.rate,
      frequency: this.props.posting.frequency,
      ownerId: this.props.posting.uid,
    });
  }


  render() {
    const today = new Date().toISOString().split('T')[0];
    return (
      <div className={styles.form}>
        <h1 className={styles.title}>{this.props.posting.title}</h1>
        <div className={styles.container}>
          <div className={classnames(styles.column, styles.leftColumn, styles.form)}>
            <img src={this.props.posting.images} alt="" />
          </div>
          <div className={classnames(styles.column, styles.rightColumn, styles.form)}>
            <label>Owner:
              <Link to={`/information/${this.props.posting.uid}`}>{this.props.posting.ownerName}</Link>
            </label>
            <label>Description: {this.props.posting.description}</label>
            {/* <p>Latitude: {this.props.posting.latitude}</p>
            <p>Longitude: {this.props.posting.longitude}</p> */}
            <label>Address: <br />
              {`${this.props.posting.street}\n${this.props.posting.city}, ${this.props.posting.state}\n${this.props.posting.zip}`}
            </label>
            <label>Capacity: {this.props.posting.capacity}</label>
            <label>Rate: {this.props.posting.rate}</label>
            <label>Frequency: {getFrequencyString(this.props.posting.frequency)}</label>
            <form className={styles.form} role="form" onSubmit={this.handleSubmit}>
              <div className="form-group">
                <label htmlFor="startDate">Start Date</label>
                <input
                  type="date"
                  name="startDate"
                  onChange={this.handleInputChange}
                  value={this.state.startDate}
                  min={today}
                  required
                />
              </div>
              <div className="form-group">
                <label htmlFor="endDate">End Date</label>
                <input
                  type="date"
                  name="endDate"
                  onChange={this.handleInputChange}
                  value={this.state.endDate}
                  min={this.state.startDate}
                  required
                /> <br />
                <button type="submit">Apply</button>
              </div>
            </form>
          </div>
        </div>
      </div>
    );
  }
}


Posting.propTypes = {
  onSubmit: React.PropTypes.func.isRequired,
  onSetPosting: React.PropTypes.func.isRequired,
  onGetPosting: React.PropTypes.func.isRequired,
  postings: React.PropTypes.arrayOf(
    React.PropTypes.shape({
      title: React.PropTypes.string,
      ownerName: React.PropTypes.string,
      description: React.PropTypes.string,
      images: React.PropTypes.string,
      // latitude: React.PropTypes.string,
      // longitude: React.PropTypes.string,
      street: React.PropTypes.string,
      city: React.PropTypes.string,
      state: React.PropTypes.string,
      zip: React.PropTypes.string,
      capacity: React.PropTypes.string,
      rate: React.PropTypes.string,
      frequency: React.PropTypes.string,
      uid: React.PropTypes.string,
    }),
  ),
  posting: React.PropTypes.shape({
    title: React.PropTypes.string,
    ownerName: React.PropTypes.string,
    description: React.PropTypes.string,
    images: React.PropTypes.string,
    // latitude: React.PropTypes.string,
    // longitude: React.PropTypes.string,
    street: React.PropTypes.string,
    city: React.PropTypes.string,
    state: React.PropTypes.string,
    zip: React.PropTypes.string,
    capacity: React.PropTypes.string,
    rate: React.PropTypes.string,
    frequency: React.PropTypes.string,
    uid: React.PropTypes.string,
  }).isRequired,
  params: React.PropTypes.shape({
    postingId: React.PropTypes.string.isRequired,
  }).isRequired,
  token: React.PropTypes.string.isRequired,
};

Posting.defaultProps = {
  postings: null,
};
