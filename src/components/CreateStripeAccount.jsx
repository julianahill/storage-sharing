import React from 'react';
import stripeClient from '../utilities/stripe';
import styles from '../styles/CreateStripeAccount.css';

export default class CreateStripeAccount extends React.Component {
  componentWillMount() {
    const query = this.props.location.query;
    if (query.code) {
      this.props.saveStripeAccount(query.code, this.props.token);
    }
  }
  render() {
    const stripeAuthlink = `https://connect.stripe.com/oauth/authorize?response_type=code&client_id=${stripeClient.clientId}&scope=read_write`;
    return (
      <div
        style={{
          display: 'flex',
          maxWidth: '32em',
          margin: '0 auto',
          marginTop: '50px',
          flexDirection: 'column',
        }}
      >
        <h1>You need to connect your account on stripe with us to list a posting</h1>
        <a href={stripeAuthlink} className={styles.stripeButton}>
          <span>Connect with Stripe</span>
        </a>
      </div>
    );
  }
}

CreateStripeAccount.propTypes = {
  location: React.PropTypes.shape({
    query: React.PropTypes.object.isRequired,
  }).isRequired,
  saveStripeAccount: React.PropTypes.func.isRequired,
  token: React.PropTypes.string.isRequired,
};
