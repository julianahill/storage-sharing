import React from 'react';
import styles from '../styles/CreditCardForm.css';

export default class CreditCardForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      expMonth: props.expMonth,
      expYear: props.expYear,
      cardNumber: props.cardNumber,
      cvc: '',
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }


  handleInputChange(event) {
    const { value, name } = event.target;
    this.setState({
      [name]: value,
    });
  }

  handleSubmit(event) {
    event.preventDefault();
    const card = {
      expMonth: parseInt(this.state.expMonth, 10),
      expYear: parseInt(this.state.expYear, 10),
      cardNumber: this.state.cardNumber,
      cvc: this.state.cvc,
    };
    this.props.saveCreditCard(card, this.props.token);
  }

  render() {
    return (
      <form className={styles.form} role="form">
        <label htmlFor="expMonth">Expiration Month</label>
        <input
          type="text"
          name="expMonth"
          onChange={this.handleInputChange}
          value={this.state.expMonth}
        />
        <br />
        <label htmlFor="expYear">Expiration Year</label>
        <input
          type="text"
          name="expYear"
          onChange={this.handleInputChange}
          value={this.state.expYear}
        />
        <br />
        <label htmlFor="cardNumber">Credit Card Number</label>
        <input
          type="text"
          name="cardNumber"
          onChange={this.handleInputChange}
          value={this.state.cardNumber}
        />
        <br />
        <label htmlFor="cvc">CVC</label>
        <input
          type="text"
          name="cvc"
          onChange={this.handleInputChange}
          value={this.state.cvc}
        />
        <br />
        <button onClick={this.handleSubmit}>Save</button>
      </form>
    );
  }
}

CreditCardForm.propTypes = {
  expMonth: React.PropTypes.string,
  expYear: React.PropTypes.string,
  cardNumber: React.PropTypes.string,
  token: React.PropTypes.string.isRequired,
  saveCreditCard: React.PropTypes.func.isRequired,
};

CreditCardForm.defaultProps = {
  expMonth: '',
  expYear: '',
  cvc: '',
  cardNumber: '',
};
